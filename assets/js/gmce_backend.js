(function($) {

    GMCommunityEditorBox.loadAdminBox = function(post) {
        var $box = $('#gmce_admin_box .inside');
        if (!$box.length) {
            return false;
        }
        $box.html('<p>' + GMCommunityEditorBoxData.loading + '</p>');
        $.ajax({
            data: {
                action: 'print_admin_box',
                gmce_post: post
            }
        }).done(function(data) {
            $box.html(data);
        }).fail(function() {
            $box.html(GMCommunityEditorBoxData.reload_error);
        });
    };

    GMCommunityEditorBox.cleanUp = function(action, dir) {
        var post = $('#gmce_adminbox').data('post');
        $.ajax({
            data: {action: action, gmce_post: post, gmce_clean: dir}
        }).done(function(data) {
            if (data === 'done') {
                $(document).trigger('gmce_' + action, post);
                GMCommunityEditorBox.loadAdminBox(post);
                var $wrap = $('#gmce_admin_votebox .inside');
                var $container = $wrap.children('#gmce_revisions_box, #gmce_vote_box').eq(0);
                if (!$container.length)
                    return false;
                $container.html('<p>' + GMCommunityEditorBoxData.loading + '</p>');
                $.ajax({
                    data: {action: 'print_box', gmce_post: post}
                }).done(function(data) {
                    $new = $(data);
                    GMCommunityEditorBoxCache.votes = $new.clone();
                    $container.replaceWith($new);
                }).fail(function() {
                    $container.html(GMCommunityEditorBoxData.reload_error);
                });
            } else {
                $(document).trigger('gmce_' + action + '_fail', post);
            }
        }).fail(function() {
            $(document).trigger('gmce_' + action + '_fail', post);
        });
    };

    $(document).on('click', '#clean_up_all, #clean_up_approvals, #clean_up_revisions', function(e) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        var id = $(this).attr('id');
        if (id === 'clean_up_all') {
            var action = id;
            var dir = null;
        } else {
            var action = 'clean_up_votes';
            var dir = (id === 'clean_up_approvals') ? 'up' : 'down';
        }
        GMCommunityEditorBox.cleanUp(action, dir);
    });

    $(document).on('gmce_returningData_done', function(obj, eventData) {
        GMCommunityEditorBox.loadAdminBox(eventData.post);
    });

    $(document).on('gmce_fix_revision_done', function(obj, eventData) {
        GMCommunityEditorBox.loadAdminBox(eventData.post);
    });

    $(document).on('gmce_unfix_revision_done', function(obj, eventData) {
        GMCommunityEditorBox.loadAdminBox(eventData.post);
    });


})(jQuery);