(function($) {

    GMCommunityEditorBox = {};

    GMCommunityEditorBoxCache = {votes: null, revs: null, tmout: []};

    tb_position = function() {
        var tbWindow = $('#TB_window');
        var h = $(window).height();
        if ($('#wpadminbar').length) {
            h -= $('#wpadminbar').outerHeight();
        }
        if (tbWindow.size()) {
            tbWindow.height(h - 45);
            $('#TB_ajaxContent').height(h - 90);
            if (typeof document.body.style.maxWidth) {
                var v = tbWindow.width();
                tbWindow.css({'margin-left': '-' + parseInt(((v - 50) / 2), 10) + 'px'});
                tbWindow.css({'top': '20px', 'margin-top': '10px'});
            }
        }
        ;
    };

    $(window).resize(function() {
        if (!GMCommunityEditorBox.isBackend) {
            tb_position();
        }
    });



    GMCommunityEditorBox.resetCache = function(which) {
        if (!which)
            which = 'all';
        if (which !== 'revs')
            GMCommunityEditorBoxCache.votes = null;
        if (which !== 'votes')
            GMCommunityEditorBoxCache.revs = null;
        $(document).trigger('gmce_resetCache', which);
    };


    GMCommunityEditorBox.resetFeedback = function(selector) {
        clearTimeout(GMCommunityEditorBox[ 'timeoutFeedback' + selector ]);
        $(selector).hide().empty().removeClass('error').removeClass('success');
        $(document).trigger('gmce_resetFeedback');
    };


    GMCommunityEditorBox.resetDisabled = function(element) {
        setTimeout(function() {
            element.data('disabled', null);
        }, 3000);
    };


    GMCommunityEditorBox.errorMsg = function(type, selector) {
        if (type) {
            $(document).trigger('gmce_errorMsg', type);
            var msg = false;
            if (type.indexOf('.') !== -1) {
                var type_split = type.split('.');
                var index = type_split[0];
                var msgs = window.GMCommunityEditorBoxData[index];
                var msg = msgs[type_split[1]];
            } else {
                msg = window.GMCommunityEditorBoxData[type];
            }
            if (!msg) {
                alert('Unknown Error!');
                return false;
            }
            GMCommunityEditorBox[ 'timeoutFeedback' + selector ] = setTimeout(function() {
                GMCommunityEditorBox.resetFeedback(selector);
            }, 6000);
            $(selector).hide().empty().removeClass('success').addClass('error').html(msg).show();
        } else {
            alert('Unknown Error!');
        }
        return false;
    };


    GMCommunityEditorBox.moveTextareaHere = function(reason) {
        var $textarea = $('#gmce_vote_box_deny_wrap');
        $textarea.show().appendTo(reason.closest('li'));
    };


    GMCommunityEditorBox.returningData = function(data, which, post, inrevisions) {
        if (inrevisions) {
            var feedback = '#gmce_revisions_feedback';
            var container = '#gmce_revisions_box';
            var box_action = 'print_rev_box';
            var cache = 'revs';
        } else {
            var container = '#gmce_vote_box';
            var feedback = '#gmce_vote_box_feedback';
            var box_action = 'print_box';
            var cache = 'votes';
        }
        var $container = $(container);
        var status = data.status;
        var message = data.message;
        if (status && message && which && post) {
            $(document).trigger('gmce_returningData_start', {data: data, which: which, post: post, is_rev: inrevisions});
            $container.html('<p>' + GMCommunityEditorBoxData.loading + '</p>');
            $.ajax({
                data: {action: box_action, gmce_post: post}
            }).done(function(data) {
                $(document).trigger('gmce_returningData_done', {data: data, which: which, post: post, is_rev: inrevisions});
                $new = $(data);
                GMCommunityEditorBoxCache[cache] = $new.clone();
                var $target = $new.find(feedback);
                $target.addClass(status).html(message).show();
                $container.replaceWith($new);
                $(document).trigger('gmce_returningData_print', {data: data, which: which, post: post, is_rev: inrevisions});
                $(document).trigger('gmce_' + box_action);
            }).fail(function() {
                $(document).trigger('gmce_returningData_fail', {data: data, which: which, post: post, is_rev: inrevisions});
                GMCommunityEditorBox.errorMsg('no_ajax_msg', feedback);
            });
        } else {
            $(document).trigger('gmce_returningData_error');
            GMCommunityEditorBox.errorMsg('general_error_msg', feedback);
        }
    };


    GMCommunityEditorBox.commentResponse = function(data) {
        var status = data.status;
        var post = data.post;
        var revision = data.revision;
        var message = data.message;
        GMCommunityEditorBox.resetFeedback('#gmce_comments_feedback');
        if (status && message && post && revision) {
            $(document).trigger('gmce_commentResponse_start', {data: data});
            $.ajax({
                data: {action: 'comments_popup', gmce_post: post, gmce_rev: revision}
            }).done(function(data) {
                $(document).trigger('gmce_commentResponse_done', {post: post, revision: revision});
                $new = $(data);
                var $target = $new.find('#gmce_comments_feedback');
                $target.addClass(status).html(message).show();
                $('#gmce_comments_wrapper').replaceWith($new);
                $(document).trigger('gmce_commentResponse_print');
            }).fail(function() {
                $(document).trigger('gmce_commentResponse_fail', {post: post, revision: revision});
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_comments_feedback');
            });
        } else {
            $(document).trigger('gmce_commentResponse_error');
            GMCommunityEditorBox.errorMsg('general_error_msg', '#gmce_comments_feedback');
        }
    };

    GMCommunityEditorBox.setScoreHelp = function(post, score_a) {
        $.ajax({
            data: {action: 'create_score_help', gmce_post: post, getjson: 1}
        }).done(function(data) {
            if ((data.status === 'success') && data.help) {
                score_a.attr('title', data.help);
            }
        });
    };


    GMCommunityEditorBox.minRevision = function(rev) {
        if (rev.hasClass('gmce_hidden'))
            return false;
        var padbot = rev.css('padding-bottom');
        rev.data('padbot', padbot);
        rev.css('padding-bottom', '8px');
        rev.find('.gmce_revision_fixed_btn').hide();
        rev.find('.gmce_revision_content').hide();
        rev.find('.gmce_revision_meta').hide();
        rev.find('a.gmce_rev_close').text(GMCommunityEditorBoxData.expand_rev);
        var author = rev.find('.gmce_revision_author a').text();
        var $author = $('<small class="gmce_added_author">' + author + '</small>');
        if (!rev.hasClass('final') && rev.find('.gmce_revision_thumbs').length) {
            rev.find('.gmce_revision_thumbs').hide();
            var score = rev.find('.gmce_revision_thumbs .thumb_score').text().trim();
            var $score = $('<small class="gmce_added_score">(' + score + ')</small>');
            rev.find('h4.gmce_revision_reason').append($score).append(' ').append($author);
        } else {
            rev.find('h4.gmce_revision_reason').append($author);
        }
        rev.addClass('gmce_hidden');
        $(document).trigger('gmce_minRevision', rev.attr('id'));
    };


    GMCommunityEditorBox.expRevision = function(rev) {
        if (!rev.hasClass('gmce_hidden'))
            return false;
        var padbot = rev.data('padbot');
        rev.css('padding-bottom', padbot);
        rev.find('h4.gmce_revision_reason .gmce_added_author').remove();
        rev.find('a.gmce_rev_close').text(GMCommunityEditorBoxData.close_rev);
        rev.find('.gmce_revision_fixed_btn').show();
        rev.find('.gmce_revision_content').show();
        rev.find('.gmce_revision_meta').show();
        if (!rev.hasClass('final') && rev.find('.gmce_revision_thumbs').length) {
            rev.find('h4.gmce_revision_reason .gmce_added_score').remove();
            rev.find('.gmce_revision_thumbs').show();
        }
        rev.removeClass('gmce_hidden');
        $(document).trigger('gmce_minRevision', rev.attr('id'));
    };


    GMCommunityEditorBox.minRevisions = function(elements) {
        if (!elements)
            elements = $('#gmce_revisions .gmce_revision');
        elements.each(function() {
            if (!$(this).hasClass('gmce_hidden'))
                GMCommunityEditorBox.minRevision($(this));
        });
        $(document).trigger('gmce_minRevisions');
    };


    GMCommunityEditorBox.expRevisions = function() {
        $('#gmce_revisions .gmce_revision').each(function() {
            if ($(this).hasClass('gmce_hidden'))
                GMCommunityEditorBox.expRevision($(this));
        });
        $(document).trigger('gmce_expRevisions');
    };


    GMCommunityEditorBox.openConfirm = function(conf, msg) {
        $(document).trigger('gmce_openConfirm_start', {conf: conf, msg: msg});
        $('.gmce_action_confirm').each(function() {
            GMCommunityEditorBox.closeConfirm($(this));
        });
        conf.find('.gmce_action_confirm_msg').html(msg);
        conf.show();
        $(document).trigger('gmce_openConfirm', {conf: conf, msg: msg});
        var tmoutid = 'confirm_' + conf.attr('id');
        GMCommunityEditorBoxCache.tmout[tmoutid] = setTimeout(function() {
            if (conf.is(':visible'))
                conf.hide();
            GMCommunityEditorBoxCache.tmout[tmoutid] = null;
        }, 10000);
    };


    GMCommunityEditorBox.closeConfirm = function(conf) {
        var tmoutid = 'confirm_' + conf.attr('id');
        var tmout = GMCommunityEditorBoxCache.tmout[tmoutid];
        if (tmout)
            clearInterval(tmout);
        GMCommunityEditorBoxCache.tmout[tmoutid] = null;
        conf.hide();
        conf.find('.gmce_action_confirm_msg').empty();
        $(document).trigger('gmce_closeConfirm', conf.attr('id'));
    };

    GMCommunityEditorBox.getType = function(obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    };

    GMCommunityEditorBox.hasClass = function(el, cl) {
        var classes = el.attr('class').split(' ');
        return $.inArray(cl, classes) !== -1;
    };

    GMCommunityEditorBox.thumbVoteInfo = function(thumb) {
        var $svg = thumb.children('svg').eq(0);
        var voted = GMCommunityEditorBox.hasClass($svg, 'voted');
        var action = thumb.hasClass('thumb-up') ? 'up' : 'down';
        var dir = (action === 'up') ? 1 : -1;
        var $score = thumb.siblings('.thumb_score');
        var score = Number($score.text());
        if (voted)
            dir *= -1;
        return {
            svg_el: $svg,
            voted: voted,
            action: action,
            dir: dir,
            score: score,
            new_score: score + dir,
            score_el: $score
        };
    };

    GMCommunityEditorBox.thumbVote = function(thumb, info) {
        if (!info)
            info = GMCommunityEditorBox.thumbVoteInfo(thumb);
        var base_class = 'icon';
        if (info.action === 'up') {
            base_class += ' icon-like';
        } else {
            base_class += ' icon-dislike';
        }
        info.score_el.text(info.new_score);
        if (info.new_score !== 0) {
            info.score_el.addClass(info.action);
        } else {
            info.score_el.removeClass('up');
            info.score_el.removeClass('down');
        }
        var msgs = GMCommunityEditorBoxData['thumbs_msg'];
        if (info.voted) {
            thumb.attr('title', msgs['vote_' + info.action]);
            info.svg_el.attr('class', base_class);
        } else {
            var other = info.action === 'up' ? 'down' : 'up';
            var $thumb_other = thumb.siblings('.thumb-' + other);
            var $svg_other = $thumb_other.children('svg').eq(0);
            if (GMCommunityEditorBox.hasClass($svg_other, 'voted')) {
                thumb.attr('title', msgs['vote_' + info.action]);
                $thumb_other.attr('title', msgs['vote_' + other]);
                info.svg_el.attr('class', base_class);
                $svg_other.attr('class', base_class);
            } else {
                thumb.attr('title', msgs['already_' + info.action]);
                info.svg_el.attr('class', base_class + ' voted voted-' + info.action);
            }
        }
    };



    $(document).ready(function() {
        // cache
        if ($('#gmce_vote_box').length) {
            GMCommunityEditorBoxCache.votes =
                    $('#gmce_vote_box').clone().wrap('<p>').parent().html();
        } else if ($('#gmce_revisions_box').length) {
            GMCommunityEditorBoxCache.revs =
                    $('#gmce_revisions_box').clone().wrap('<p>').parent().html();
        }
        // is admin
        GMCommunityEditorBox.isBackend = ($('body').hasClass('wp-admin')) ? true : false;
        // reset inputs
        if ($('ul.gmce_approve_reasons').length) {
            $('ul.gmce_approve_reasons input[type="radio"]').each(function() {
                $(this).attr('checked', false);
            });
        } else if ($('#gmce_revisions').length) {
            $('#gmce_revisions .gmce_revision').each(function() {
                if (!$(this).hasClass('fixed')) {
                    $(this).find('input[type="checkbox"]').eq(0).attr('checked', false);
                }
            });
        }
    });

    $(document).on('click', '.gmce_approve_btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        $button.data('disabled', 1);
        GMCommunityEditorBox.resetCache();
        var post = $button.data('post');
        GMCommunityEditorBox.resetFeedback('#gmce_vote_box_feedback');
        $('input[name="gmce_deny_reason"]:checked').attr('checked', false);
        $('input[name="gmce_deny_reason_content"]').val('');
        $.ajax({
            data: {action: 'approve_vote', gmce_post: post, getjson: 1}
        }).done(function(data) {
            $(document).trigger('gmce_approve_vote', post);
            GMCommunityEditorBox.returningData(data, 'approve', post);
        }).fail(function() {
            $(document).trigger('gmce_approve_vote_fail', post);
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_vote_box_feedback');
        });
    });

    $(document).on('click', '#gmce_approvers_show_details', function(e) {
        e.preventDefault();
        var $list = $('#gmce_approvers_list');
        if ($list.is(':visible')) {
            var txt = GMCommunityEditorBoxData.show;
            var $conf = $('#gmce_remove_approval_confirm');
            $conf.data('uid', null);
            GMCommunityEditorBox.closeConfirm($conf);
        } else {
            var txt = GMCommunityEditorBoxData.hide;
        }
        $(this).text('(' + txt + ')');
        $list.toggle();
    });


    $(document).on('click', 'a.gmce_approve_delete', function(e) {
        e.preventDefault();
        var $user = $(this).closest('.gmce_approver').find('.gmce_approver_link a').eq(0);
        var username = $user.text();
        var uid = $user.data('uid');
        var $conf = $('#gmce_remove_approval_confirm');
        $conf.data('uid', uid);
        var msg = GMCommunityEditorBoxData.delete_approve_confirm.replace('%s', username);
        GMCommunityEditorBox.openConfirm($conf, msg);
    });


    $(document).on('click', '.gmce_remove_approval_yesno a', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $conf = $(this).closest('.gmce_action_confirm');
        var uid = $conf.data('uid');
        if (!uid)
            return false;
        var yesno = $(this).data('confirm');
        var post = $('#gmce_vote_box').data('post');
        GMCommunityEditorBox.closeConfirm($conf);
        $conf.data('uid', null);
        if (Number(yesno) === 1) {
            $.ajax({
                data: {
                    action: 'delete_approval',
                    gmce_post: post,
                    gmce_uid: uid,
                    getjson: 1
                }
            }).done(function(data) {
                GMCommunityEditorBox.resetCache();
                $(document).trigger('gmce_remove_approval', {post: post, uid: uid});
                GMCommunityEditorBox.returningData(data, 'delete_approval', post);
            }).fail(function() {
                $(document).trigger('gmce_remove_approval_fail', {post: post, uid: uid});
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_revisions_feedback');
            });
        }
    });


    $(document).on('change', 'input[name="gmce_deny_reason"]', function() {
        if ($('#gmce_vote_box_feedback').hasClass('error'))
            GMCommunityEditorBox.resetFeedback('#gmce_vote_box_feedback');
        GMCommunityEditorBox.moveTextareaHere($(this));
    });


    $(document).on('keydown', 'textarea[name="gmce_final_revision_content"]', function() {
        if ($('#gmce_revisions_feedback').hasClass('error'))
            GMCommunityEditorBox.resetFeedback('#gmce_revisions_feedback');
    });


    $(document).on('keydown', 'textarea[name="gmce_deny_reason_content"]', function() {
        if ($('#gmce_vote_box_feedback').hasClass('error'))
            GMCommunityEditorBox.resetFeedback('#gmce_vote_box_feedback');
    });


    $(document).on('click', '#gmce_vote_box_deny_wrap .gmce_deny_btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        GMCommunityEditorBox.resetCache();
        var post = $button.data('post');
        GMCommunityEditorBox.resetFeedback('#gmce_vote_box_feedback');
        var reason = $('input[name="gmce_deny_reason"]:checked').length
                ? $('input[name="gmce_deny_reason"]:checked').val()
                : false;
        if (!reason) {
            GMCommunityEditorBox.errorMsg('no_reason_msg', '#gmce_vote_box_feedback');
            return false;
        }
        var reason_content = $('textarea[name="gmce_deny_reason_content"]').val();
        reason_content = reason_content.replace(/(<([^>]+)>)/ig, '');
        if (reason_content.length === 0) {
            GMCommunityEditorBox.errorMsg('no_reason_content_msg', '#gmce_vote_box_feedback');
            return false;
        } else if (reason_content.length < 20) {
            GMCommunityEditorBox.errorMsg('reason_content_length_msg', '#gmce_vote_box_feedback');
            return false;
        }
        $('textarea[name="gmce_deny_reason_content"]').val('');
        $button.data('disabled', 1);
        $.ajax({
            data: {
                action: 'revise_vote',
                gmce_post: post,
                gmce_reason: reason,
                gmce_reason_content: reason_content,
                getjson: 1
            }
        }).done(function(data) {
            $(document).trigger('gmce_revision_vote', {post: post, reason: reason});
            GMCommunityEditorBox.returningData(data, 'deny', post);
        }).fail(function() {
            $(document).trigger('gmce_revision_vote_fail', {post: post, reason: reason});
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_vote_box_feedback');
        });
    });


    $(document).on('click', '#gmce_final_revision_wrap .gmce_deny_btn', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        GMCommunityEditorBox.resetCache();
        $button.hide();
        var post = $button.data('post');
        GMCommunityEditorBox.resetFeedback('#gmce_revisions_feedback');
        var reason_content = $('textarea[name="gmce_final_revision_content"]').val();
        reason_content = reason_content.replace(/(<([^>]+)>)/ig, '');
        if (reason_content.length === 0) {
            GMCommunityEditorBox.errorMsg('no_reason_content_msg', '#gmce_revisions_feedback');
            return false;
        } else if (reason_content.length < 20) {
            GMCommunityEditorBox.errorMsg('reason_content_length_msg', '#gmce_revisions_feedback');
            return false;
        }
        $('textarea[name="gmce_final_revision_content"]').val('');
        $button.data('disabled', 1);
        $.ajax({
            data: {
                action: 'revise_vote',
                gmce_post: post,
                gmce_reason: '_final_revision_',
                gmce_reason_content: reason_content,
                getjson: 1
            }
        }).done(function(data) {
            $(document).trigger('gmce_revision_final_vote', post);
            GMCommunityEditorBox.returningData(data, 'deny', post, true);
            $button.data('disabled', null);
            $button.show();
        }).fail(function() {
            $(document).trigger('gmce_revision_final_vote_fail', post);
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_revisions_feedback');
        });
    });



    $(document).on('change', '.gmce_revision_fixed_btn input[type="checkbox"]', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        $button.data('disabled', 1);
        GMCommunityEditorBox.resetCache();
        $button.parent().hide();
        var $label = $button.parent().find('label').eq(0);
        var $score_a = $('#gmce_score_wrap');
        $score_a.attr('title', '');
        var action = $button.prop('checked') ? 'fix_revision' : 'unfix_revision';
        var $score = $('#gmce_score');
        var score = Number($score.text());
        var $rev = $button.closest('.gmce_revision');
        var revid = $button.val();
        var is_admin = $button.closest('#gmce_revisions').data('isadmin');
        if (!is_admin)
            $button.parent().html(GMCommunityEditorBoxData.fixed).show();
        if (action === 'fix_revision' && (!$rev.hasClass('fixed'))) {
            $rev.addClass('fixed');
            score += 2;
            var limit = Number($score.data('limit'));
            var clss = (limit === score) ? 'score_done' : 'score_' + score;
            $('#gmce_score_wrap').attr('class', clss);
            $label.text($label.text().replace('?', ''));
        } else if ($rev.hasClass('fixed')) {
            $rev.removeClass('fixed');
            score -= 2;
            $('#gmce_score_wrap').attr('class', 'score_' + score);
            $label.text($label.text() + '?');
        } else {
            if ($rev.hasClass('fixed')) {
                $button.attr('checked', true);
            } else {
                $button.attr('checked', false);
            }
            $button.data('disabled', null);
            return false;
        }
        var $unfixed_count = $('.gmce_revision_summary_title span');
        var unfixed_num = $('.gmce_revision').not('.fixed').length;
        var unfixed_txt = GMCommunityEditorBoxData.unfixed + ': ' + unfixed_num;
        $unfixed_count.text(unfixed_txt);
        if (unfixed_num === 0) {
            $unfixed_count.removeClass('unfixed_count');
        } else {
            $unfixed_count.addClass('unfixed_count');
        }
        $score.text(score);
        $.ajax({
            data: {action: action, gmce_rev: revid}
        }).done(function() {
            var post = $('#gmce_revisions_box').data('post');
            $(document).trigger('gmce_' + action + '_done', {revid: revid, post: post});
            if (is_admin) {
                $button.parent().show();
                $button.data('disabled', null);
            }
            if (post)
                GMCommunityEditorBox.setScoreHelp(post, $score_a);
        });
    });


    $(document).on('click', 'a#gmce_rev_close_all', function(e) {
        e.preventDefault();
        GMCommunityEditorBox.minRevisions();
        var $exp = '<a href="#" id="gmce_rev_exp_all">' + GMCommunityEditorBoxData.expand_all_rev
                + '</a>';
        $(this).replaceWith($exp);
    });


    $(document).on('click', 'a#gmce_rev_close_fixed', function(e) {
        e.preventDefault();
        var $el = $('#gmce_revisions .fixed');
        GMCommunityEditorBox.minRevisions($el);
        if ($('a#gmce_rev_close_all').length) {
            var $exp = '<a href="#" id="gmce_rev_exp_all">'
                    + GMCommunityEditorBoxData.expand_all_rev + '</a>';
            $('a#gmce_rev_close_all').replaceWith($exp);
        }
    });


    $(document).on('click', 'a#gmce_rev_exp_all', function(e) {
        e.preventDefault();
        GMCommunityEditorBox.expRevisions();
        var $cls = '<a href="#" id="gmce_rev_close_all">' + GMCommunityEditorBoxData.close_all_rev
                + '</a>';
        $(this).replaceWith($cls);
    });


    $(document).on('click', 'a.gmce_rev_close', function(e) {
        e.preventDefault();
        var $rev = $(this).closest('.gmce_revision');
        if ($rev.hasClass('gmce_hidden')) {
            $(this).text(GMCommunityEditorBoxData.close_rev);
            GMCommunityEditorBox.expRevision($rev);
        } else {
            $(this).text(GMCommunityEditorBoxData.expand_rev);
            GMCommunityEditorBox.minRevision($rev);
        }
    });


    $(document).on('click', 'a.gmce_rev_delete', function(e) {
        e.preventDefault();
        var $conf = $(this).closest('.gmce_revision').find('.gmce_action_confirm').eq(0);
        var msg = GMCommunityEditorBoxData.delete_rev_confirm;
        GMCommunityEditorBox.openConfirm($conf, msg);
    });




    $(document).on('click', '.gmce_remove_rev_yesno a', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $conf = $(this).closest('.gmce_action_confirm');
        var yesno = $(this).data('confirm');
        var post = $('#gmce_revisions_box').data('post');
        var uid = $conf.data('author');
        var revid = $conf.data('id');
        GMCommunityEditorBox.closeConfirm($conf);
        if (Number(yesno) === 1) {
            $.ajax({
                data: {
                    action: 'delete_revision',
                    gmce_post: post,
                    gmce_rev: revid,
                    getjson: 1
                }
            }).done(function(data) {
                GMCommunityEditorBox.resetCache();
                $(document).trigger('gmce_revision_remove', {revid: revid, post: post, uid: uid});
                GMCommunityEditorBox.returningData(data, 'delete_revision', post, true);
                $(this).closest('.gmce_revision').remove();
            }).fail(function() {
                $(document).trigger('gmce_revision_remove_fail', {revid: revid, post: post, uid: uid});
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_revisions_feedback');
            });
        }
    });




    $(document).on('click', 'a#gmce_show_rev_link', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        if (GMCommunityEditorBoxCache.revs) {
            $('#gmce_vote_box').replaceWith(GMCommunityEditorBoxCache.revs);
            return false;
        }
        $button.data('disabled', 1);
        var post = $button.data('post');
        var $now = $('#gmce_vote_box').html();
        $('#gmce_vote_box').empty().append('<p>' + GMCommunityEditorBoxData.loading + '</p>');
        $.ajax({
            data: {action: 'print_rev_box', gmce_post: post}
        }).done(function(data) {
            if (data && $('#gmce_vote_box').length) {
                var $html = $(data);
                $html.find('#gmce_revisions .gmce_revision').each(function() {
                    if (!$(this).hasClass('fixed')) {
                        $(this).find('input[type="checkbox"]').eq(0).attr('checked', false);
                    }
                });
                $('#gmce_vote_box').replaceWith($html);
                GMCommunityEditorBoxCache.revs = $html;
                $(document).trigger('gmce_print_rev_box');
            } else {
                $('#gmce_vote_box').html($now);
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_vote_box_feedback');
            }
        }).fail(function() {
            $('#gmce_vote_box').html($now);
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_vote_box_feedback');
        });
    });


    $(document).on('click', 'a#gmce_show_vote_link', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        if (GMCommunityEditorBoxCache.votes) {
            $('#gmce_revisions_box').replaceWith(GMCommunityEditorBoxCache.votes);
            return false;
        }
        $button.data('disabled', 1);
        var post = $button.data('post');
        var $now = $('#gmce_revisions_box').html();
        $('#gmce_revisions_box').empty().append('<p>' + GMCommunityEditorBoxData.loading
                + '</p>');
        $.ajax({
            data: {action: 'print_box', gmce_post: post}
        }).done(function(data) {
            if (data && $('#gmce_revisions_box').length) {
                $('#gmce_revisions_box').replaceWith(data);
                GMCommunityEditorBoxCache.votes = data;
            } else {
                $('#gmce_revisions_box').html($now);
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_vote_box_feedback');
            }
            $(document).trigger('gmce_print_box');
        }).fail(function() {
            $('#gmce_revisions_box').html($now);
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_vote_box_feedback');
        });
    });


    $(document).ready(function() {
        var $checked = $('input[name="gmce_deny_reason"]:checked').eq(0);
        if ($checked.length)
            GMCommunityEditorBox.moveTextareaHere($checked);
    });



    /* Thumb votes */

    $(document).on('click', '.gmce_revision_thumbs a.thumb', function(e) {
        e.preventDefault();
        var $thumbs = $(this).closest('.gmce_revision_thumbs');
        if ($thumbs.data('disabled'))
            return false;
        $thumbs.data('disabled', 1);
        var $thumb = $(this);
        var info = GMCommunityEditorBox.thumbVoteInfo($(this));
        GMCommunityEditorBox.resetFeedback('#gmce_revisions_feedback');
        var action = 'thumb_' + info.action;
        var revision = $(this).closest('.gmce_revision').data('revision');
        $.ajax({
            data: {
                action: action,
                gmce_rev: revision
            }
        }).done(function(data) {
            if (data === 'success') {
                GMCommunityEditorBox.resetCache('revs');
                $(document).trigger('gmce_revision_' + action, revision);
                GMCommunityEditorBox.thumbVote($thumb, info);
            } else if (data.indexOf('error_') === 0) {
                if (data === 'error_banned' || data === 'error_went_banned') {
                    $('#gmce_revisions .gmce_revision').css('padding-bottom', '28px');
                    $('.gmce_revision_thumbs').remove();
                }
                $(document).trigger('gmce_revision_' + action + '_fail', revision, data);
                GMCommunityEditorBox.errorMsg('thumbs_msg.' + data, '#gmce_revisions_feedback');
            } else {
                $(document).trigger('gmce_revision_' + action + '_fail', revision);
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_revisions_feedback');
            }
        }).fail(function() {
            $(document).trigger('gmce_revision_' + action + '_fail', revision);
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_revisions_feedback');
        });
        GMCommunityEditorBox.resetDisabled($thumbs);
    });

    $(document).on('click', '.gmce_revision_thumbs_voters h6 a', function(e) {
        e.preventDefault();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        $button.data('disabled', 1);
        var rev = $(this).closest('.gmce_revision').data('revision');
        var post = $('#gmce_revisions_box').data('post');
        var h = $(window).height() - 100;
        if (GMCommunityEditorBox.isBackend) {
            h -= 20;
        }
        var qv = '&action=thumb_votes_popup&gmce_post=' + post + '&gmce_rev=' + rev + '&height=' + h;
        if (!rev || !post) {
            GMCommunityEditorBox.errorMsg('general_error_msg', '#gmce_revisions_feedback');
            GMCommunityEditorBox.resetDisabled($button);
            return false;
        }
        var tb_show_url = easy_ajax_vars.ajaxurl + qv;
        $(document).trigger('thumb_votes_popup_open', {rev: rev});
        tb_show('', tb_show_url);
        $(document).trigger('thumb_votes_popup_opened', {rev: rev});
        GMCommunityEditorBox.resetDisabled($button);
    });



    /* Comments */

    $(window).resize(function() {
        if ($("#TB_ajaxContent").length && $('.gmce_popup_wrapper').length) {
            var h = $(window).height() - 100;
            if (GMCommunityEditorBox.isBackend) {
                h -= 20;
                $('#TB_ajaxContent').height(h);
            }
            $('.gmce_popup_wrapper').height(h).css({height: h + 'px'});
        }
    });

    $(document).on('click', '#gmce_comment_link', function(e) {
        e.preventDefault();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        $button.data('disabled', 1);
        var post = $(this).data('post');
        var rev = $(this).data('rev');
        var h = $(window).height() - 100;
        if (GMCommunityEditorBox.isBackend) {
            h -= 20;
        }
        var qv = '&action=comments_popup&gmce_post=' + post + '&gmce_rev=' + rev + '&height=' + h;
        if (!post || !rev) {
            GMCommunityEditorBox.errorMsg('general_error_msg', '#gmce_revisions_feedback');
            GMCommunityEditorBox.resetDisabled($button);
            return false;
        }
        var tb_show_url = easy_ajax_vars.ajaxurl + qv;
        $(document).trigger('gmce_comment_open', {post: post, rev: rev});
        tb_show('', tb_show_url);
        $(document).trigger('gmce_comment_opened', {post: post, rev: rev});
        GMCommunityEditorBox.resetDisabled($button);
    });

    $(document).on('click', '.gmce_leave_comment_open', function(e) {
        e.preventDefault();
        GMCommunityEditorBox.resetFeedback('#gmce_comments_feedback');
        $('#gmce_comment_content_wrap').removeClass('closed').find('textarea').focus();
    });

    $(document).on('click', '#gmce_leave_comment_close', function(e) {
        e.preventDefault();
        GMCommunityEditorBox.resetFeedback('#gmce_comments_feedback');
        $('#gmce_comment_content_wrap').addClass('closed');
    });

    $(document).on('click', '#gmce_comments_original_show a', function(e) {
        e.preventDefault();
        GMCommunityEditorBox.resetFeedback('#gmce_comments_feedback');
        $('#gmce_comments_original').slideToggle();
        if ($(this).hasClass('show')) {
            $(this).removeClass('show').text(GMCommunityEditorBoxData.hide_original);
        } else {
            $(this).addClass('show').text(GMCommunityEditorBoxData.show_original);
        }

    });

    $(document).on('click', '#gmce_submit_comment', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        GMCommunityEditorBox.resetFeedback('#gmce_comments_feedback');
        var inputs = $('#gmce_add_comment_form').serializeArray();
        var form_data = {};
        $(inputs).each(function(i, el) {
            if (el.value)
                form_data[el.name] = el.value;
        });
        if (!form_data.gmce_post || !form_data.gmce_rev) {
            GMCommunityEditorBox.errorMsg('comment_no_data_msg', '#gmce_comments_feedback');
            return false;
        } else if (!form_data.gmce_comment) {
            GMCommunityEditorBox.errorMsg('comment_no_content_msg', '#gmce_comments_feedback');
            return false;
        }
        $('#gmce_add_comment_form textarea').val('');
        form_data['action'] = 'add_comment';
        form_data['getjson'] = 1;
        $button.data('disabled', 1);
        $.ajax({
            data: form_data
        }).done(function(data) {
            if (data) {
                GMCommunityEditorBox.commentResponse(data);
            } else {
                GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_comments_feedback');
            }
            GMCommunityEditorBox.resetDisabled($button);
        }).fail(function() {
            GMCommunityEditorBox.errorMsg('no_ajax_msg', '#gmce_comments_feedback');
        });

    });

    $(document).on('click', '.gmce_comment_delete a', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $button = $(this);
        if ($button.data('disabled'))
            return false;
        $button.data('disabled', 1);
        var $content = $button.closest('.gmce_comment').find('.gmce_comment_content');
        var cid = $button.data('comment');
        $content.empty().append('<em class="deleted">' + GMCommunityEditorBoxData.deleted
                + '</em>');
        $(this).parent().remove();
        $.ajax({data: {action: 'delete_comment', gmce_comment: cid}});
        $(document).trigger('gmce_comment_deleted', cid);
    });

    $(document).on('click', '#gmce_comments_sort a', function(e) {
        e.preventDefault();
        var neworder = ($(this).data('order') === 'datedesc') ? 'asc' : 'desc';
        $(this).data('order', 'date' + neworder);
        $(this).text(GMCommunityEditorBoxData['orderby_date' + neworder]);
        $('ul#gmce_comments_list>li').tsort('', {data: 'tmstmp', order: neworder});
    });



    /* Reason Templates */

    $(document).on('click', '#gmce_comment_templates', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $textarea = $('textarea[name="gmce_deny_reason_content"]');
        var value = $textarea.val();
        if (value !== '')
            value += '\n';
        $textarea.val(value + $(this).parent().find('select').eq(0).val());
    });

    $(document).on('click', '#gmce_comment_clean', function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('textarea[name="gmce_deny_reason_content"]').val('').css({height: '5em'});
    });


})(jQuery);