msgid ""
msgstr ""
"Project-Id-Version: GM Community Editors v1.2.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2014-03-31 16:43:36+0000\n"
"Last-Translator: zoom <giuseppe.mazzapica@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: CSL v1.x\n"
"X-Poedit-Language: English\n"
"X-Poedit-Country: UNITED STATES\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n"
"X-Poedit-Basepath: \n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-SearchPath-0: .\n"
"X-Textdomain-Support: yes"

#: configs.php:9
#: src/Installer.php:30
#: src/Output/Backend.php:117
#@ gmce
msgid "Community Editor"
msgstr ""

#: configs.php:13
#: settings/messages.php:5
#: settings/messages.php:12
#@ gmce
msgid "Approve"
msgstr ""

#: configs.php:14
#: settings/messages.php:6
#: settings/messages.php:13
#@ gmce
msgid "Revise"
msgstr ""

#: configs.php:15
#: settings/messages.php:14
#@ gmce
msgid "Undo Revision"
msgstr ""

#: reasons.php:5
#@ gmce
msgid "Typos"
msgstr ""

#: reasons.php:6
#@ gmce
msgid "There are typos in the post content."
msgstr ""

#: reasons.php:10
#@ gmce
msgid "Bad Format"
msgstr ""

#: reasons.php:11
#@ gmce
msgid "Post contains bad punctuation, or wrong use of caps."
msgstr ""

#: reasons.php:14
#@ gmce
msgid "Unclear"
msgstr ""

#: reasons.php:15
#@ gmce
msgid "Post content is unclear."
msgstr ""

#: reasons.php:18
#@ gmce
msgid "Bad Images"
msgstr ""

#: reasons.php:19
#@ gmce
msgid "Images are missing, or poor quality or bad."
msgstr ""

#: reasons.php:22
#@ gmce
msgid "Copied"
msgstr ""

#: reasons.php:23
#@ gmce
msgid "Post is copied and source isn't linked nor cited."
msgstr ""

#: reasons.php:26
#@ gmce
msgid "Bad Tags"
msgstr ""

#: reasons.php:27
#@ gmce
msgid "Tags in post are wrong."
msgstr ""

#: reasons.php:30
#@ gmce
msgid "Bad Title"
msgstr ""

#: reasons.php:31
#@ gmce
msgid "Title in post is wrong."
msgstr ""

#: reasons.php:34
#: reasons.php:38
#@ gmce
msgid "Offensive"
msgstr ""

#: reasons.php:35
#: reasons.php:39
#@ gmce
msgid "Post is offensive and/or does not respect the netiquette."
msgstr ""

#: reasons.php:42
#@ gmce
msgid "Other"
msgstr ""

#: reasons.php:43
#@ gmce
msgid "Please explain reason in revision comment."
msgstr ""

#: helpers.php:33
#@ gmce
msgid "Few seconds ago"
msgstr ""

#: helpers.php:35
#, php-format
#@ gmce
msgid "%s ago"
msgstr ""

#: helpers.php:39
#, php-format
#@ gmce
msgctxt "On date"
msgid "on %s"
msgstr ""

#: settings/messages.php:3
#@ gmce
msgid "Yes"
msgstr ""

#: settings/messages.php:4
#@ gmce
msgid "No"
msgstr ""

#: settings/messages.php:7
#@ gmce
msgid "Users Voted"
msgstr ""

#: settings/messages.php:8
#@ gmce
msgid "Locked"
msgstr ""

#: settings/messages.php:9
#: settings/messages.php:148
#@ gmce
msgid "Score"
msgstr ""

#: settings/messages.php:10
#@ gmce
msgid "Post is locked, no approve / deny will be accepted untill an admin will unlock it."
msgstr ""

#: settings/messages.php:11
#@ gmce
msgid "User"
msgid_plural "Users"
msgstr[0] ""
msgstr[1] ""

#: settings/messages.php:15
#, php-format
#@ gmce
msgid "Post approved by %s"
msgstr ""

#: settings/messages.php:16
#@ gmce
msgid "No one have approved this post."
msgstr ""

#: settings/messages.php:17
#@ gmce
msgid "Please, add a comment to your revision"
msgstr ""

#: settings/messages.php:18
#@ gmce
msgid "Please, add your final revision"
msgstr ""

#: settings/messages.php:19
#, php-format
#@ gmce
msgid "Post revised by %s."
msgstr ""

#: settings/messages.php:20
#@ gmce
msgid "No one have revised this post."
msgstr ""

#: settings/messages.php:21
#@ gmce
msgid "Show Revisions"
msgstr ""

#: settings/messages.php:22
#: settings/messages.php:96
#@ gmce
msgid "Show Vote Box"
msgstr ""

#: settings/messages.php:23
#, php-format
#@ gmce
msgid "You have already revised this post %s."
msgstr ""

#: settings/messages.php:24
#, php-format
#@ gmce
msgid "You said post issue type was: %s, and commented: %s"
msgstr ""

#: settings/messages.php:25
#@ gmce
msgid "Your revision was marked as fixed by yourself, or by post author or by an administrator."
msgstr ""

#: settings/messages.php:26
#@ gmce
msgid "You've already approved this post."
msgstr ""

#: settings/messages.php:27
#@ gmce
msgid "You can't revise or approve your own post."
msgstr ""

#: settings/messages.php:28
#@ gmce
msgctxt "Approvals and Revisions arrows"
msgid "&#8593; / &#8595;"
msgstr ""

#: settings/messages.php:29
#@ gmce
msgid " &raquo; Approve or Revise"
msgstr ""

#: settings/messages.php:30
#@ gmce
msgid " &raquo; Check Approves and Revisions"
msgstr ""

#: settings/messages.php:31
#, php-format
#@ gmce
msgid "Approvals (%d)"
msgstr ""

#: settings/messages.php:32
#, php-format
#@ gmce
msgid "Revisions (%d)"
msgstr ""

#: settings/messages.php:33
#, php-format
#@ gmce
msgid "Unfixed Revisions (%d)"
msgstr ""

#: settings/messages.php:34
#, php-format
#@ gmce
msgctxt "%1$s: post title, %2$s: post author name, %3$s: editor name, %4$s: reason"
msgid "Post %1$s (by %2$s) revised by %3$s: %4$s"
msgstr ""

#: settings/messages.php:35
#@ gmce
msgid "There are no community revisions for this post, at the moment."
msgstr ""

#: settings/messages.php:37
#@ gmce
msgid "Please, select a reason checking a radio button."
msgstr ""

#: settings/messages.php:38
#@ gmce
msgid "Please, leave a comment to your revision."
msgstr ""

#: settings/messages.php:39
#@ gmce
msgid "Please, comment to revision have to be at least 20 chars long."
msgstr ""

#: settings/messages.php:40
#@ gmce
msgid "Sorry, an error occured on performing ajax request."
msgstr ""

#: settings/messages.php:41
#: settings/messages.php:70
#@ gmce
msgid "Sorry, unknown error occured."
msgstr ""

#: settings/messages.php:42
#@ gmce
msgid "Perfect! Required operation performed successfully."
msgstr ""

#: settings/messages.php:43
#@ gmce
msgid "Loading..."
msgstr ""

#: settings/messages.php:44
#@ gmce
msgid "Hide Original Post Content"
msgstr ""

#: settings/messages.php:45
#: settings/messages.php:106
#@ gmce
msgid "Show Original Post Content"
msgstr ""

#: settings/messages.php:46
#@ gmce
msgid "Sorry, error on saving comment: missing data."
msgstr ""

#: settings/messages.php:47
#@ gmce
msgid "Please, do not submit empty comments."
msgstr ""

#: settings/messages.php:48
#@ gmce
msgid "Fixed"
msgstr ""

#: settings/messages.php:50
#@ gmce
msgid "Deleted"
msgstr ""

#: settings/messages.php:51
#@ gmce
msgid "Delete"
msgstr ""

#: settings/messages.php:52
#@ gmce
msgid "Date Order &uarr;"
msgstr ""

#: settings/messages.php:53
#@ gmce
msgid "Date Order &darr;"
msgstr ""

#: settings/messages.php:54
#@ gmce
msgid "close all"
msgstr ""

#: settings/messages.php:55
#@ gmce
msgid "close fixed"
msgstr ""

#: settings/messages.php:56
#@ gmce
msgid "expand all"
msgstr ""

#: settings/messages.php:57
#@ gmce
msgid "&minus;"
msgstr ""

#: settings/messages.php:58
#@ gmce
msgid "+"
msgstr ""

#: settings/messages.php:59
#: settings/messages.php:61
#@ gmce
msgid "&times;"
msgstr ""

#: settings/messages.php:60
#@ gmce
msgid "Delete Revision"
msgstr ""

#: settings/messages.php:62
#@ gmce
msgid "Delete Approval"
msgstr ""

#: settings/messages.php:63
#@ gmce
msgid "Are you sure to delete this revision?"
msgstr ""

#: settings/messages.php:64
#, php-format
#@ gmce
msgid "Are you sure to delete approval from %s?"
msgstr ""

#: settings/messages.php:65
#@ gmce
msgid "Sorry, an error occured, please reload current page."
msgstr ""

#: settings/messages.php:66
#@ gmce
msgid "show"
msgstr ""

#: settings/messages.php:67
#@ gmce
msgid "hide"
msgstr ""

#: settings/messages.php:71
#@ gmce
msgid "Sorry, you have already casted this vote."
msgstr ""

#: settings/messages.php:72
#@ gmce
msgid "Sorry, you have already revised this post."
msgstr ""

#: settings/messages.php:73
#@ gmce
msgid "Sorry, you can vote only one time per post."
msgstr ""

#: settings/messages.php:74
#@ gmce
msgid "Sorry, you have already voted 2 times (first approved then revised) this post."
msgstr ""

#: settings/messages.php:75
#@ gmce
msgid "Sorry, no vote can be accepted for locked posts."
msgstr ""

#: settings/messages.php:76
#@ gmce
msgid "Sorry, you are not allowed to perform the requested operation."
msgstr ""

#: settings/messages.php:77
#@ gmce
msgid "Sorry, you are not allowed to approve or deny your own post."
msgstr ""

#: settings/messages.php:78
#@ gmce
msgid "Your approval was registered. Remember to leave a comment to this post."
msgstr ""

#: settings/messages.php:79
#@ gmce
msgid "Your Revision was registered."
msgstr ""

#: settings/messages.php:80
#@ gmce
msgid "Approval removed."
msgstr ""

#: settings/messages.php:81
#@ gmce
msgid "Revision removed."
msgstr ""

#: settings/messages.php:82
#@ gmce
msgid "Your Revision was undone."
msgstr ""

#: settings/messages.php:83
#@ gmce
msgid "Sorry, an error occurred on adding your comment."
msgstr ""

#: settings/messages.php:84
#@ gmce
msgid "Comment added successfully."
msgstr ""

#: settings/messages.php:86
#@ gmce
msgid "Community Revisions"
msgstr ""

#: settings/messages.php:87
#, php-format
#@ gmce
msgid "Approved %d time."
msgid_plural "Approved %d times."
msgstr[0] ""
msgstr[1] ""

#: settings/messages.php:88
#@ gmce
msgid "Revision summary"
msgstr ""

#: settings/messages.php:89
#: settings/messages.php:143
#@ gmce
msgid "Revisions"
msgstr ""

#: settings/messages.php:90
#@ gmce
msgid "Revision feedback by Editors"
msgstr ""

#: settings/messages.php:91
#, php-format
#@ gmce
msgid "Post has %d final revision."
msgid_plural "Post has %d final revisions."
msgstr[0] ""
msgstr[1] ""

#: settings/messages.php:92
#@ gmce
msgid "Final Revision"
msgstr ""

#: settings/messages.php:93
#@ gmce
msgid "Total"
msgstr ""

#: settings/messages.php:49
#: settings/messages.php:94
#@ gmce
msgid "Unfixed"
msgstr ""

#: settings/messages.php:97
#: settings/messages.php:104
#@ gmce
msgid "Comments"
msgstr ""

#: settings/messages.php:98
#@ gmce
msgid "Leave a Comment"
msgstr ""

#: settings/messages.php:99
#@ gmce
msgid "Revision:"
msgstr ""

#: settings/messages.php:100
#@ gmce
msgid "No comment at the moment."
msgstr ""

#: settings/messages.php:101
#@ gmce
msgid "Be the first."
msgstr ""

#: settings/messages.php:102
#@ gmce
msgid "Your comment here..."
msgstr ""

#: settings/messages.php:103
#@ gmce
msgid "(&times; close)"
msgstr ""

#: settings/messages.php:105
#@ gmce
msgid "Submit"
msgstr ""

#: settings/messages.php:107
#@ gmce
msgctxt "Before original post author in comment popup"
msgid " &mdash; by"
msgstr ""

#: settings/messages.php:108
#@ gmce
msgid "Editors"
msgstr ""

#: settings/messages.php:109
#, php-format
#@ gmce
msgid "Score must be %d before post can be published."
msgstr ""

#: settings/messages.php:110
#@ gmce
msgid "Post reached the required score. After the final check by an editor it will be published, if no further issues will be found."
msgstr ""

#: settings/messages.php:111
#, php-format
#@ gmce
msgid "The %d unfixed revision must be fixed to increment score."
msgstr ""

#: settings/messages.php:112
#, php-format
#@ gmce
msgid "%d more users need to approve the post."
msgstr ""

#: settings/messages.php:139
#@ gmce
msgid "Informations"
msgstr ""

#: settings/messages.php:140
#@ gmce
msgid "Actions"
msgstr ""

#: settings/messages.php:141
#, php-format
#@ gmce
msgid "%d / %d Users Voted"
msgstr ""

#: settings/messages.php:142
#@ gmce
msgid "Approvals"
msgstr ""

#: settings/messages.php:144
#@ gmce
msgid "Final Revisions"
msgstr ""

#: settings/messages.php:145
#@ gmce
msgid "Unfixed Revisions"
msgstr ""

#: settings/messages.php:146
#@ gmce
msgid "Approvers"
msgstr ""

#: settings/messages.php:147
#@ gmce
msgid "Revisers"
msgstr ""

#: settings/messages.php:149
#@ gmce
msgid "Is post locked?"
msgstr ""

#: settings/messages.php:150
#@ gmce
msgid "GM Community Editor Admin Tools"
msgstr ""

#: settings/messages.php:151
#@ gmce
msgid "Clear Approvals"
msgstr ""

#: settings/messages.php:152
#@ gmce
msgid "Clear Revisions"
msgstr ""

#: settings/messages.php:153
#@ gmce
msgid "Clear All"
msgstr ""

#: settings/messages.php:95
#@ gmce
msgid "Unfixed revisions"
msgstr ""

#: settings/messages.php:114
#@ gmce
msgid "You've already voted up. Click again to undo."
msgstr ""

#: settings/messages.php:115
#@ gmce
msgid "You've already voted down. Click again to undo."
msgstr ""

#: settings/messages.php:116
#@ gmce
msgid "Vote Up"
msgstr ""

#: settings/messages.php:117
#@ gmce
msgid "Vote Down"
msgstr ""

#: settings/messages.php:118
#, php-format
#@ gmce
msgid "Last %d Upvoters"
msgstr ""

#: settings/messages.php:119
#@ gmce
msgid "Last Voter"
msgstr ""

#: settings/messages.php:120
#@ gmce
msgid "Last Upvoter"
msgstr ""

#: settings/messages.php:121
#, php-format
#@ gmce
msgid "Last %d Voters"
msgstr ""

#: settings/messages.php:122
#@ gmce
msgid "View All"
msgstr ""

#: settings/messages.php:123
#@ gmce
msgid "Click to show info on voters"
msgstr ""

#: settings/messages.php:124
#@ gmce
msgid "You can't vote your own revision."
msgstr ""

#: settings/messages.php:125
#@ gmce
msgid "Sorry, you have to wait at least 10 seconds between 2 votes on same revision. Counter resetted."
msgstr ""

#: settings/messages.php:126
#@ gmce
msgid "Sorry, your vote was not registered because you have voted too much times in a hour same revision. Counter resetted."
msgstr ""

#: settings/messages.php:127
#@ gmce
msgid "Sorry, you cant vote because you were banned probably for having voted too much times in a hour same revision."
msgstr ""

#: settings/messages.php:128
#@ gmce
msgid "Sorry, your vote was not registered and you were banned for having voted too much times in a hour same revision."
msgstr ""

#: settings/messages.php:129
#@ gmce
msgid "Sorry, post authors can't vote revisions on their own posts."
msgstr ""

#: settings/messages.php:130
#, php-format
#@ gmce
msgid "Revision got %d upvotes and %d downvotes so current thumb-votes score is: %d"
msgstr ""

#: settings/messages.php:131
#@ gmce
msgid "Upvoters:"
msgstr ""

#: settings/messages.php:132
#@ gmce
msgid "Downvoters:"
msgstr ""

#: settings/messages.php:133
#@ gmce
msgid "Revision Thumb Votes Info"
msgstr ""

#: settings/messages.php:135
#, php-format
#@ gmce
msgid "Revision to post &quot;%s&quot; by %s"
msgstr ""

#: settings/messages.php:136
#, php-format
#@ gmce
msgid "Revision posted by %s %s"
msgstr ""

#: settings/messages.php:137
#@ gmce
msgid "Revision reason: "
msgstr ""

#: src/Output/Backend.php:118
#@ gmce
msgid "Thumb Votes"
msgstr ""

#: src/Output/Backend.php:120
#@ gmce
msgid "Ban from revisions thumb votes"
msgstr ""

#: src/Output/Backend.php:125
#, php-format
#@ gmce
msgid "auto banned on %s"
msgstr ""

#: src/Output/Backend.php:128
#@ gmce
msgid "manually banned"
msgstr ""

#: src/Output/BoxWidget.php:15
#@ gmce
msgid "Community Editor widget, only for preview or pending posts."
msgstr ""

#: src/ReasonsTemplater.php:61
#@ gmce
msgid "Add"
msgstr ""

#: src/ReasonsTemplater.php:63
#@ gmce
msgid "Reset"
msgstr ""

