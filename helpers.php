<?php

function gmce_get_input( $name, $method = 'get', $type = 'string', $raw = FALSE ) {
    $verb = ( strtolower( $method ) === 'get' ) ? INPUT_GET : INPUT_POST;
    if ( strtolower( $type ) === 'int' ) {
        $filter = FILTER_SANITIZE_NUMBER_INT;
        $opt = null;
    } else {
        $filter = $raw ? FILTER_UNSAFE_RAW : FILTER_SANITIZE_STRING;
        $opt = $raw ? null : FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW;
    }
    return filter_input( $verb, $name, $filter, $opt );
}

function gmce_get_get( $name, $type = 'string', $raw = FALSE ) {
    return gmce_get_input( $name, 'get', $type, $raw );
}

function gmce_get_post( $name, $type = 'string', $raw = FALSE ) {
    return gmce_get_input( $name, 'post', $type, $raw );
}

function gmce_get_request( $name, $type = 'string', $raw = FALSE ) {
    $var = gmce_get_input( $name, 'post', $type, $raw );
    if ( is_null( $var ) ) $var = gmce_get_input( $name, 'get', $type, $raw );
    return $var;
}

function gmce_human_post_time( $id = 0, $comment = FALSE, $format = NULL ) {
    $time = FALSE;
    if ( is_object( $comment ) && isset( $comment->comment_date_gmt ) ) {
        $time = mysql2date( 'U', $comment->comment_date_gmt );
    } elseif ( is_numeric( $id ) && (int) $id > 0 ) {
        $time = get_post_time( 'U', TRUE, $id );
    }
    if ( ! $time ) return;
    $now = current_time( 'timestamp', TRUE );
    $time_diff = (int) $now - (int) $time ? : 1;
    if ( $time_diff < 60 ) {
        return __( 'Few seconds ago', 'gmce' );
    } elseif ( $time_diff < DAY_IN_SECONDS ) {
        return sprintf( __( '%s ago', 'gmce' ), human_time_diff( $time, $now ) );
    } else {
        $date_format = $format ? : get_option( 'date_format' );
        $offset = get_option( 'gmt_offset' ) * HOUR_IN_SECONDS;
        $date = date_i18n( $date_format, ( $time + $offset ) );
        return sprintf( _x( 'on %s', 'On date', 'gmce' ), $date );
    }
}
