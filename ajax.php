<?php
error_reporting( 0 );
define( 'SHORTINIT', TRUE );
define( 'DOING_AJAX', TRUE );
define( 'WP_ADMIN', TRUE );
define( 'DISABLE_WP_CRON', TRUE );
define( 'WP_AUTO_UPDATE_CORE', FALSE );
define( 'WP_DEBUG_DISPLAY', FALSE );
$path = base64_decode( filter_input( INPUT_GET, 'p', FILTER_SANITIZE_STRING ) );
require_once $path . '/wp-load.php';
remove_action( 'init', 'check_theme_switched', 99 );
remove_action( 'init', 'wp_cron' );
remove_action( 'init', 'smilies_init', 5 );
remove_action( 'plugins_loaded', 'wp_maybe_load_widgets', 0 );
remove_action( 'plugins_loaded', 'wp_maybe_load_embeds', 0 );
remove_action( 'admin_init', 'register_admin_color_schemes', 1 );
require_once( ABSPATH . WPINC . '/l10n.php' );
foreach ( [
'class-wp-walker', 'class-wp-ajax-response', 'formatting', 'capabilities', 'query', 'date',
 'theme', 'template', 'user', 'meta', 'general-template', 'link-template', 'author-template',
 'post', 'post-template', 'revision', 'category', 'category-template', 'comment',
 'comment-template', 'rewrite', 'kses', 'cron', 'taxonomy', 'shortcodes', 'http', 'class-http',
 'widgets'
] as $file ) {
    require ABSPATH . WPINC . "/{$file}.php";
}
if ( is_multisite() ) require( ABSPATH . WPINC . '/ms-functions.php' );
wp_plugin_directory_constants();
foreach ( wp_get_mu_plugins() as $mu_plugin ) {
    include_once $mu_plugin;
}
unset( $mu_plugin );
do_action( 'muplugins_loaded' );
if ( is_multisite() ) ms_cookie_constants();
wp_cookie_constants();
wp_ssl_constants();
$GLOBALS['wp_plugin_paths'] = array();
if ( ! function_exists( 'wp_normalize_path' ) ) {

    function wp_normalize_path( $path ) {
        return str_replace( '\\', '/', preg_replace( '|/+|', '/', $path ) );
    }

}
if ( ! function_exists( 'wp_register_plugin_realpath' ) ) {

    function wp_register_plugin_realpath( $file ) {
        global $wp_plugin_paths;
        static $wp_plugin_path, $wpmu_plugin_path;
        if ( ! isset( $wp_plugin_path ) ) {
            $wp_plugin_path = wp_normalize_path( WP_PLUGIN_DIR );
            $wpmu_plugin_path = wp_normalize_path( WPMU_PLUGIN_DIR );
        }
        $plugin_path = wp_normalize_path( dirname( $file ) );
        $plugin_realpath = wp_normalize_path( dirname( realpath( $file ) ) );
        if ( $plugin_path === $wp_plugin_path || $plugin_path === $wpmu_plugin_path ) {
            return false;
        }
        if ( $plugin_path !== $plugin_realpath ) {
            $wp_plugin_paths[$plugin_path] = $plugin_realpath;
        }
        return true;
    }

}

if ( is_multisite() ) {
    foreach ( wp_get_active_network_plugins() as $network_plugin ) {
        wp_register_plugin_realpath( $network_plugin );
        include_once( $network_plugin );
    }
    unset( $network_plugin );
}
$gmce = str_replace( 'ajax.php', 'plugin.php', wp_normalize_path( __FILE__ ) );
wp_register_plugin_realpath( $gmce );
require_once $gmce;
$supported_plugins = apply_filters( 'gmce_supported_plugins', [ ] );
if ( ! empty( $supported_plugins ) ) {
    foreach ( wp_get_active_and_valid_plugins() as $plugin ) {
        $plugin = wp_normalize_path( $plugin );
        if ( $plugin === $gmce ) continue;
        wp_register_plugin_realpath( $plugin );
        if ( ! in_array( plugin_basename( $plugin ), $supported_plugins ) ) continue;
        include_once $plugin;
        unset( $plugin );
    }
}
unset( $gmce, $supported_plugins );
require( ABSPATH . WPINC . '/pluggable.php' );
wp_set_internal_encoding();
if ( WP_CACHE && function_exists( 'wp_cache_postload' ) ) wp_cache_postload();
do_action( 'plugins_loaded' );
wp_functionality_constants();
wp_magic_quotes();
do_action( 'sanitize_comment_cookies' );
$GLOBALS['wp_rewrite'] = new WP_Rewrite();
$wp = new WP();
$GLOBALS['wp_roles'] = new WP_Roles();
do_action( 'setup_theme' );
wp_templating_constants();
load_default_textdomain();
$locale = get_locale();
$locale_file = WP_LANG_DIR . "/{$locale}.php";
if ( ( 0 === validate_file( $locale ) ) && is_readable( $locale_file ) ) require( $locale_file );
unset( $locale_file );
require_once( ABSPATH . WPINC . '/locale.php' );
$GLOBALS['wp_locale'] = new WP_Locale();
do_action( 'after_setup_theme' );
$wp->init();
do_action( 'init' );
if ( is_multisite() ) {
    if ( TRUE !== ( $file = ms_site_check() ) ) {
        require( $file );
        die();
    } unset( $file );
}
do_action( 'wp_loaded' );
send_origin_headers();
$action_get = filter_input( INPUT_GET, 'action', FILTER_SANITIZE_STRING );
$action_post = filter_input( INPUT_POST, 'action', FILTER_SANITIZE_STRING );
$action = empty( $action_post ) ? $action_get : $action_post;
if ( empty( $action ) ) die( '0' );
require_once( ABSPATH . 'wp-admin/includes/admin.php' );
require_once( ABSPATH . 'wp-admin/includes/ajax-actions.php' );
header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
header( 'X-Robots-Tag: noindex' );
send_nosniff_header();
nocache_headers();
do_action( 'admin_init' );
$priv = is_user_logged_in() ? '' : 'nopriv_';
do_action( 'wp_ajax_' . $priv . $action );
die( '0' );
