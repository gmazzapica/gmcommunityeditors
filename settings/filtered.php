<?php
return [
    'role_name'            => 'community_editor',
    'box_css'              => NULL,
    'rev_post_type_public' => NULL,
    'allowed_html'         => [
        'p'      => [ ], 'br'     => [ ],
        'em'     => [ ], 'i'      => [ ], 'strong' => [ ], 'b'      => [ ], 'del'    => [ ],
        'hr'     => [ ],
    ],
    'post_html'            => [
        'p'      => [ ], 'br'     => [ ],
        'em'     => [ ], 'i'      => [ ], 'strong' => [ ], 'b'      => [ ], 'del'    => [ ],
        'hr'     => [ ], 'ul'     => [ ], 'ol'     => [ ], 'li'     => [ ],
        'a'      => [ 'href' => [ ], 'title' => [ ], 'target' => [ ] ]
    ]
];
