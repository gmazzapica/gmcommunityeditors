<?php
return [
    'configs.php'               => NULL,
    'reasons.php'               => 'reasons',
    'reason_templates.php'      => 'reason_templates',
    'settings/filtered.php'     => NULL,
    'settings/messages.php'     => 'messages',
    'settings/ajax_actions.php' => 'ajax_actions'
];
