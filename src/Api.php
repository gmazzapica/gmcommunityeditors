<?php

namespace GMCE;

use Brain\Container as Brain;

class Api {

    function revs_as_posts( $revs = [ ] ) {
        if ( ! empty( $revs ) && \is_array( $revs ) ) {
            $revs = \array_map( [ $this, 'rev_as_post' ], $revs );
        }
        return $revs;
    }

    function rev_as_post( $rev = '' ) {
        if ( $rev instanceof \GMCE\Revision ) {
            $post = $rev->as_post();
            unset( $post->format_content );
            return $post;
        }
    }

    public function get_revs( $args = [ ] ) {
        $revs = Brain::instance()->get( 'revisions' );
        $def = [
            'posts'         => NULL,
            'user'          => NULL,
            'status'        => 'publish',
            'exclude_final' => TRUE
        ];
        $args = wp_parse_args( $args, $def );
        $result = $revs->get( $args[ 'posts' ], $args[ 'user' ], $args[ 'status' ], $args[ 'exclude_final' ] );
        if ( ! empty( $result ) ) $result = $this->revs_as_posts( $result );
        return $result;
    }

    public function get_post_revs( $post = 0, $status = 'publish', $no_final = TRUE ) {
        $revs = Brain::instance()->get( 'revisions' );
        $result = $revs->post_revs( $post, $status, $no_final );
        if ( ! empty( $result ) ) $result = $this->revs_as_posts( $result );
        return $result;
    }

    public function get_user_revs( $user = 0, $status = 'publish', $no_final = TRUE ) {
        $revs = Brain::instance()->get( 'revisions' );
        $result = $revs->user_revs( $user, $status, $no_final );
        if ( ! empty( $result ) ) $result = $this->revs_as_posts( $result );
        return $result;
    }

    public function get_post_votes( $post = 0, $which = NULL ) {
        $v = Brain::instance()->get( 'votes' );
        $votes = $v->votes_info( $post );
        $k = [ 'approved' => 'approvals', 'denied' => 'revisions', 'denyers' => 'revisers' ];
        foreach ( (array) $votes as $i => $v ) {
            if ( in_array( $i, array_keys( $k ) ) ) {
                $votes[ $k[ $i ] ] = $votes[ $i ];
                unset( $votes[ $i ] );
            }
        }
        if ( ! is_string( $which ) ) return $votes;
        if ( isset( $votes[ $which ] ) ) {
            return $votes[ $which ];
        }
    }

    public function get_user_votes( $user = 0, $which = NULL ) {
        if ( is_numeric( $user ) ) $user = (int) $user;
        if ( is_int( $user ) ) $user = new \WP_User( $user );
        if ( ! $user instanceof \WP_User ) return FALSE;
        if ( ! (int) $user->ID > 0 ) return FALSE;
        $votes = get_user_meta( $user->ID, '__gmce_votes', TRUE );
        if ( empty( $votes ) ) return [ ];
        $k = [
            'up'      => 'approvals_now', 'down'    => 'revisions_now', 'total'   => 'total_now',
            'up_s'    => 'approvals', 'down_s'  => 'revisions', 'total_s' => 'total'
        ];
        foreach ( $votes as $i => $v ) {
            if ( in_array( $i, array_keys( $k ) ) ) {
                $votes[ $k[ $i ] ] = $v;
                unset( $votes[ $i ] );
            }
        }
        if ( ! is_string( $which ) ) return $votes;
        if ( isset( $votes[ $which ] ) ) return $votes[ $which ];
    }

}