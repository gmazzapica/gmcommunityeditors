<?php

namespace GMCE;

/**
 * AjaxProxy Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class AjaxProxy extends Base\Providerable {

    protected $gmce;

    protected $frontend;

    protected $backend;

    protected $actions;

    function setup( CommunityEditor $gmce ) {
        $this->provider();
        $this->gmce = $gmce;
        $this->backend = $this->gmce->get_backend();
        $this->frontend = $this->gmce->get_frontend();
        $this->actions = $this->settings->get( 'ajax_actions', [ ] );
    }

    protected function get_post_and_setup( $backend = FALSE ) {
        $id = gmce_get_request( 'gmce_post' );
        if ( $id ) {
            $this->gmce->setup_post( $id );
            if ( ! $backend ) {
                $this->frontend->setup();
            } else {
                $this->backend->setup();
            }
        }
        return (int) $id;
    }

    protected function get_revision() {
        $revision = $this->revisions->single->get( (int) gmce_get_request( 'gmce_rev', 'int' ) );
        if ( ! $revision instanceof \GMCE\Revision )
                wp_die( $this->msg[ 'ajax_vote_reponses' ][ 'error' ] );
        return $revision;
    }

    function approve_vote() {
        return $this->votes->vote_up();
    }

    function revise_vote() {
        return $this->votes->vote_down();
    }

    function print_box() {
        $id = $this->get_post_and_setup();
        if ( ! $id ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        return $this->frontend->create_box();
    }

    function print_rev_box() {
        $id = $this->get_post_and_setup();
        if ( ! $id ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        return $this->frontend->create_revisions_box();
    }

    function print_admin_box() {
        $id = $this->get_post_and_setup( TRUE );
        if ( ! $id ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        $this->backend->setup_post();
        return $this->backend->admin_box();
    }

    function create_score_help() {
        $id = $this->get_post_and_setup();
        if ( ! $id ) return (object) [ 'status' => 'error' ];
        $help = $this->frontend->create_score_help();
        if ( $help ) return (object) [ 'status' => 'success', 'help' => $help ];
    }

    function cleanup_post() {
        $id = $this->get_post_and_setup();
        if ( $id ) {
            $post = get_post( $id );
            $this->votes->clean_up( $post, TRUE );
            return $this->msg[ 'frontend_localize' ][ 'general_success_msg' ];
        }
        return $this->msg[ 'frontend_localize' ][ 'general_error_msg' ];
    }

    function comments_popup() {
        $revision = $this->get_revision();
        $id = $this->get_post_and_setup();
        if ( ! $id ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        return $this->frontend->comments_popup( $revision );
    }

    function add_comment() {
        $revision = $this->get_revision();
        $postid = $this->get_post_and_setup();
        $comment = gmce_get_request( 'gmce_comment', 'string', TRUE );
        return $this->revisions->add_revision_comment( $revision, $postid, $comment );
    }

    function fix_revision() {
        $revision = $this->get_revision();
        return ( $revision->set_fixed() ) ? 'success' : 'error';
    }

    function unfix_revision() {
        $revision = $this->get_revision();
        return ( $revision->set_unfixed() ) ? 'success' : 'error';
    }

    function delete_comment() {
        $id = gmce_get_request( 'gmce_comment' );
        if ( $id ) return $this->revisions->single->delete_comment( $id ) ? 'success' : 'error';
        return 'error';
    }

    function delete_revision() {
        $post = $this->get_post_and_setup();
        if ( ! $post ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        $revision = $this->get_revision();
        return $this->votes->remove_vote( $post, $revision->post_author, 'down', $revision );
    }

    function delete_approval() {
        $post = $this->get_post_and_setup();
        if ( ! $post ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        $uid = (int) gmce_get_request( 'gmce_uid', 'int' );
        return $this->votes->remove_vote( $post, $uid, 'up' );
    }

    function thumb_up() {
        $revision = $this->get_revision();
        $vote = $this->thumb_votes->thumb_up( $revision->ID );
        if ( $vote === TRUE ) {
            return 'success';
        } elseif ( is_string( $vote ) ) {
            return "error_{$vote}";
        }
        return 'error';
    }

    function thumb_down() {
        $revision = $this->get_revision();
        $vote = $this->thumb_votes->thumb_down( $revision->ID );
        if ( $vote === TRUE ) {
            return 'success';
        } elseif ( is_string( $vote ) ) {
            return "error_{$vote}";
        }
        return 'error';
    }

    function thumb_votes_popup() {
        $revision = $this->get_revision();
        $id = $this->get_post_and_setup();
        if ( ! $id ) return $this->msg[ 'ajax_vote_reponses' ][ 'error' ];
        return $this->frontend->thumb_votes_popup( $revision );
    }

    function clean_up_all() {
        $post = $this->get_post_and_setup( TRUE );
        $this->votes->clean_up( $post );
        return 'done';
    }

    function clean_up_votes() {
        $post = $this->get_post_and_setup( TRUE );
        $w = gmce_get_request( 'gmce_clean' );
        $this->votes->clean_up_votes( $post, $w );
        return 'done';
    }

}