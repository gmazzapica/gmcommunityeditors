<?php namespace GMCE;

/**
 * AutoPublisher Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class AutoPublisher {

    protected static $transient = 'gmce_publish_queue';

    protected $settings;

    protected $last;

    protected $scheduling = FALSE;

    function __construct( SettingsGMCE $s ) {
        $this->settings = $s;
    }

    function enqueue( $postid = 0 ) {
        if ( ! $this->settings->get( 'autopublish_enable', FALSE ) ) return;
        if ( is_object( $postid ) && isset( $postid->ID ) ) $postid = $postid->ID;
        if ( ! is_numeric( $postid ) ) return;
        if ( get_post_status( $postid ) === 'pending' ) {
            $queue = $this->getQueue();
            $queue[$postid] = current_time( 'timestamp', TRUE );
            set_transient( static::$transient, $queue );
        }
    }

    function dequeue( $postid = 0 ) {
        if ( is_object( $postid ) && isset( $postid->ID ) ) $postid = $postid->ID;
        if ( ! is_numeric( $postid ) ) return;
        if ( $this->inQueue( $postid ) ) {
            $queue = $this->getQueue();
            unset( $queue[$postid] );
            set_transient( static::$transient, $queue );
        }
    }

    function schedule() {
        if ( $this->scheduling ) return;
        $this->scheduling = TRUE;
        $this->flushLast();
        if ( ! $this->settings->get( 'autopublish_enable', FALSE ) ) {
            $this->flushQueue();
            return;
        }
        $queue = $this->getQueue();
        if ( empty( $queue ) ) return;
        foreach ( $queue as $id => $queued ) {
            if ( $this->shouldSchedule( $queued ) ) {
                $this->schedulePost( $id );
            }
        }
        if ( ! count( $this->getQueue() ) ) $this->flushQueue();
        $this->scheduling = FALSE;
    }

    protected function schedulePost( $postid ) {
        if ( $this->inQueue( $postid ) && get_post_status( $postid ) === 'pending' ) {
            wp_update_post( $this->getPostData( $postid ) );
            $this->dequeue( $postid );
        }
    }

    protected function getPostData( $postid ) {
        $data = [ 'ID' => $postid, 'post_status' => 'future' ];
        $timestamp = (int) $this->getPublishTime( $postid );
        $now = (int) current_time( 'timestamp', TRUE );
        if ( $timestamp > $now && is_numeric( $timestamp ) ) {
            $locale = $timestamp + ( (int) get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );
            $data['post_date_gmt'] = date( 'Y-m-d H:i:s', $timestamp );
            $data['post_date'] = date( 'Y-m-d H:i:s', $locale );
        }
        return $data;
    }

    protected function shouldSchedule( $queued = 0 ) {
        $now = current_time( 'timestamp', TRUE );
        if ( empty( $queued ) || ! is_numeric( $queued ) ) $queued = $now;
        $delay = $this->settings->get( 'autopublish_queue_time', 480 );
        if ( ! is_numeric( $delay ) || (int) $delay < 5 ) $delay = 5;
        return ( (int) $now - (int) $queued ) > (int) $delay * 60;
    }

    protected function getPublishTime( $postid ) {
        $actual = (int) mysql2date( 'U', get_post_field( 'post_date', $postid, 'raw' ) );
        /* Is post date manually set to a date more than 12 hours in future? If so return 0 */
        if ( $actual > ( (int) current_time( 'timestamp', FALSE ) + ( 12 * HOUR_IN_SECONDS ) ) ) {
            return 0;
        }
        $interval = $this->settings->get( 'autopublish_min_post_interval', 120 );
        if ( ! is_numeric( $interval ) || (int) $interval < 5 ) $interval = 5;
        $now = (int) current_time( 'timestamp', TRUE );
        $last = (int) $this->getLast();
        if ( $last < $now ) $last = $now;
        $timestamp = (int) $last + ( (int) $interval * 60 );
        $this->setLast( $timestamp );
        return $timestamp;
    }

    protected function inQueue( $postid = 0 ) {
        if ( is_object( $postid ) && isset( $postid->ID ) ) $postid = $postid->ID;
        if ( ! is_numeric( $postid ) ) return;
        return array_key_exists( $postid, $this->getQueue() );
    }

    protected function getQueue() {
        return array_filter( (array) get_transient( static::$transient ) );
    }

    protected function flushQueue() {
        delete_transient( static::$transient );
    }

    protected function getLast() {
        return $this->last;
    }

    protected function setLast( $last ) {
        if ( ! is_numeric( $last ) || $last <= 0 ) $last = current_time( 'timestamp', TRUE );
        $this->last = $last;
    }

    protected function flushLast() {
        $this->last = 0;
    }

}