<?php namespace GMCE;

use \GMCE\Output as O;
use Brain\Container as Brain;

/**
 * CommunityEditor Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
final class CommunityEditor extends Base\Providerable {

    protected $backend;
    protected $frontend;
    protected $ajax;
    public $post;
    public $type_allowed;

    function __construct( O\Backend $b, O\Frontend $f, AjaxProxy $a ) {
        $this->backend = $b;
        $this->frontend = $f;
        $this->ajax = $a;
    }

    function setup() {
        $this->provider();
        $this->revisions->setup();
        $this->init_request();
    }

    function init_request() {
        if ( ! ( defined( 'DOING_AJAX' ) && \DOING_AJAX ) ) {
            $this->init_regular_request();
        } else {
            $this->init_ajax_request();
        }
    }

    public function get_backend() {
        return $this->backend;
    }

    public function get_frontend() {
        return $this->frontend;
    }

    function setup_post( $postid ) {
        $this->post = (int) $postid > 0 ? get_post( $postid ) : FALSE;
        if ( ! $this->post instanceof \WP_Post ) {
            $this->post = FALSE;
        } else {
            $this->type_allowed = $this->check_type();
            if ( $this->type_allowed ) $this->setup_user();
        }
    }

    function setup_edit_post() {
        $this->setup_post( gmce_get_get( 'post', 'int' ) );
        if ( $this->type_allowed ) {
            $this->init_easyajax();
            if ( $this->user->is_admin_or_author ) {
                $this->frontend->setup();
            }
        }
    }

    protected function init_regular_request() {
        $this->backend->setup();
        if ( is_admin() ) {
            add_action( 'load-post.php', [ $this, 'setup_edit_post' ], 1 );
        }
        Brain::instance()->get( 'reason_templater' )->setup();
        if ( $this->check_preview() ) $this->setup_preview();
    }

    protected function init_ajax_request() {
        $action = gmce_get_post( 'action' ) ? : NULL;
        if ( is_null( $action ) ) $action = gmce_get_get( 'action' ) ? : NULL;
        if ( in_array( $action, $this->settings->get( 'ajax_actions', [ ] ) ) ) {
            $this->ajax->setup( $this );
            $this->init_easyajax( $this->ajax );
        }
    }

    protected function setup_preview() {
        $postid = gmce_get_get( 'p', 'int' );
        if ( empty( $postid ) && in_array( 'page', $this->settings->get( 'post_types', [ 'post' ] ) ) ) {
            $postid = gmce_get_get( 'page_id', 'int' );
        }
        $this->setup_post( $postid );
        if ( $this->type_allowed ) {
            $this->init_easyajax( '', 'front' );
            $this->frontend->setup();
        }
    }

    protected function check_preview() {
        return ( is_admin() ) ? FALSE : (bool) gmce_get_get( 'preview' );
    }

    protected function check_type() {
        if ( empty( $this->post ) ) return FALSE;
        return in_array( $this->post->post_type, $this->settings->get( 'post_types', [ 'post' ] ) );
    }

    protected function setup_user() {
        $this->user->is_author = ( (int) $this->post->post_author === (int) $this->user->ID );
        $this->user->setup_cap( $this->post );
        $this->user->allow_preview();
    }

    protected function init_easyajax( $scope = '', $where = 'both' ) {
        /* add_filter( 'easyajax_custom_url', function() {
          return Brain::instance()->get( 'url' ) . 'ajax.php?p=' . base64_encode( ABSPATH );
          } ); */
        $path = explode( '/src/', wp_normalize_path( __FILE__ ) );
        $eafile = $path[ 0 ] . '/vendor/zoomlab/easyajax/EasyAjax.php';
        wp_register_plugin_realpath( $eafile );
        require_once $eafile;
        easyajax( $scope, $this->settings->get( 'ajax_actions', [ ] ), $where );
    }

}