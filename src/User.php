<?php namespace GMCE;

/**
 * User Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class User {

    protected $settings;
    public $user;
    public $ID;
    public $is_admin;
    public $is_ce;
    public $can_box;
    protected static $can_preview;
    public $can_vote;
    public $can_thumb_vote;
    public $is_admin_or_author;
    public $allowed_post;
    public $is_author;

    function __construct( SettingsGMCE $s ) {
        $this->settings = $s;
    }

    function __get( $name ) {
        if ( isset( $this->user->$name ) ) return $this->user->$name;
    }

    function setup() {
        $this->user = wp_get_current_user();
        if ( ! $this->user instanceof \WP_User || empty( $this->user->ID ) ) return;
        $this->ID = (int) $this->user->ID;
        $admin_cap = $this->settings->get( 'admin_cap', 'manage_options' );
        $admin_cap_ensure = $this->settings->get( 'admin_cap_ensure', 'manage_options' );
        $role = $this->settings->get( 'role_name', 'community_editor' );
        $this->is_admin = user_can( $this->user, $admin_cap ) || user_can( $this->user, $admin_cap_ensure );
        $this->is_ce = user_can( $this->user, $role );
    }

    function setup_cap( $post ) {
        $has = get_post_meta( $post->ID, '__gmce_votes', TRUE );
        $special_cap = ( $this->is_ce || $this->is_author );
        $status = ( $post->post_status === 'pending' ) || ( ( $post->post_status === 'draft' ) && $has);
        $this->can_box = $status && ( $this->is_admin || $special_cap );
        $this->can_vote = $status && ( ( $this->is_admin || $this->is_ce ) && ! $this->is_author );
        $th_vote_auth = ( ! $this->is_author ) || $this->settings->get( 'post_author_can_vote', TRUE );
        $this->can_thumb_vote = $status && ( $this->is_admin || $this->is_ce || $th_vote_auth );
        if ( $this->is_banned() ) $this->can_thumb_vote = FALSE;
        $this->is_admin_or_author = ( $this->is_admin || $this->is_author );
        self::$can_preview = ( $status && $special_cap );
        if ( self::$can_preview || $this->is_admin ) $this->allowed_post = $post;
    }

    function is_or( $ids = 0, $type = 'admin' ) {
        $is = 'is_' . $type;
        $check = ($type === 'special') ? ($this->is_admin || $this->is_ce) : (bool) $this->$is;
        if ( $check === TRUE ) return TRUE;
        if ( is_numeric( $ids ) ) return (int) $ids === $this->ID;
        return ( is_array( $ids ) && in_array( $this->ID, $ids ) );
    }

    function is_admin_or( $ids = 0 ) {
        return $this->is_or( $ids, 'admin' );
    }

    function is_ce_or( $ids = 0 ) {
        return $this->is_or( $ids, 'ce' );
    }

    function is_special_or( $ids = 0 ) {
        return $this->is_or( $ids, 'special' );
    }

    function allow_preview() {
        if ( self::$can_preview ) {
            if ( ! defined( 'DOING_AJAX' ) ) $this->allow_preview_actions();
        }
    }

    function is_banned() {
        if ( $this->is_admin ) return;
        $banned = get_user_meta( $this->ID, 'gmce_thumb_banned', TRUE );
        if ( empty( $banned ) ) return FALSE;
        return (int) $banned > 1000000000 ? 'auto' : TRUE;
    }

    function ban() {
        if ( $this->is_admin ) return;
        update_user_meta( $this->ID, 'gmce_thumb_banned', 1 );
    }

    function autoban() {
        if ( $this->is_admin ) return;
        update_user_meta( $this->ID, 'gmce_thumb_banned', current_time( 'timestamp', TRUE ) );
    }

    function unban() {
        if ( $this->is_admin ) return;
        delete_user_meta( $this->ID, 'gmce_thumb_banned' );
    }

    function allow_preview_actions() {
        add_filter( 'user_has_cap', [ __CLASS__, 'set_preview_cap' ], 999 );
        add_filter( 'template_redirect', [ __CLASS__, 'unset_preview_cap' ], 1 );
    }

    function had_voted( $votes, $which = NULL ) {
        if ( empty( $votes ) ) return FALSE;
        $finals = isset( $votes[ 'final_down_users' ] ) ? $votes[ 'final_down_users' ] : [ ];
        if ( is_null( $which ) ) {
            $up = isset( $votes[ 'up' ] ) ? in_array( $this->ID, $votes[ 'up' ] ) : FALSE;
            $down = isset( $votes[ 'down' ] ) ? in_array( $this->ID, $votes[ 'down' ] ) : FALSE;
            $final = in_array( $this->ID, $finals );
            return $up || $down || $final;
        } elseif ( in_array( $which, [ 'up', 'down', 'final' ], TRUE ) ) {
            $array = $which === 'final' ? $finals : $votes[ $which ];
            return in_array( $this->ID, $array );
        }
    }

    function had_thumb_voted( $votes, $which = NULL ) {
        if ( ! is_array( $votes ) || ! isset( $votes[ 'up' ] ) || ! isset( $votes[ 'down' ] ) ) {
            return FALSE;
        }
        if ( is_null( $which ) ) {
            return in_array( $this->ID, $votes[ 'up' ] ) || in_array( $this->ID, $votes[ 'down' ] );
        } elseif ( in_array( $which, [ 'up', 'down' ], TRUE ) ) {
            return in_array( $this->ID, $votes[ $which ] );
        }
    }

    function can_vote( $post, $up_down = NULL ) {
        if ( $up_down !== 'up' && $up_down !== 'down' ) return FALSE;
        if ( (int) $post->post_author === (int) $this->ID ) return FALSE;
        return ( ( $this->is_admin || $this->is_ce ) && ! $this->is_author );
    }

    function set_votes_info( $up_down = NULL, $op = 1, $final = FALSE ) {
        if ( $up_down !== 'up' && $up_down !== 'down' ) return;
        if ( $op !== 1 && $op !== -1 ) $op = 1;
        $k = [ 'up', 'down', 'total', 'up_s', 'down_s', 'total_s', 'final' ];
        $def = array_combine( $k, array_fill( 0, count( $k ), 0 ) );
        $user_meta = wp_parse_args( get_user_meta( $this->ID, '__gmce_votes', TRUE ), $def );
        if ( ! $final ) {
            $user_meta[ $up_down ] = $user_meta[ $up_down ] + $op;
            $user_meta[ 'total' ] = $user_meta[ 'total' ] + $op;
        } else {
            $user_meta[ 'final' ] ++;
        }
        update_user_meta( $this->ID, '__gmce_votes', $user_meta );
    }

    function posts_link() {
        $format = '<a href="%s" data-uid="%d">%s</a>';
        $url = esc_url( get_author_posts_url( $this->ID ) );
        return sprintf( $format, $url, $this->ID, esc_html( $this->display_name ) );
    }

    static function set_preview_cap( $allcaps ) {
        if ( ! is_admin() && self::$can_preview ) $allcaps[ 'edit_others_posts' ] = TRUE;
        return $allcaps;
    }

    static function unset_preview_cap() {
        self::$can_preview = FALSE;
        remove_filter( 'user_has_cap', [ __CLASS__, 'set_preview_cap' ], 999 );
    }

}