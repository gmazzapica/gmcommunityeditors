<?php namespace GMCE;

/**
 * BrainModule Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class BrainModule implements \Brain\Module {

    public function boot( \Brain\Container $brain ) {

        $reasons = $brain[ 'settings' ]->get( 'reasons', [ ] );

        if ( empty( $reasons ) ) {
            add_action( 'admin_notices', function() {
                $format = '<div class="error"><p>%s</p></div>';
                printf(
                    $format, '<h2>ERROR</h2><b>GM CommunityEditor</b> can\'t work because there are'
                    . ' no revision reasons set in <b>reasons.php</b> file.'
                );
            } );
            return;
        }

        add_action( 'after_setup_theme', function() use($brain) {
            $brain->get( 'community_editor' );
        }, PHP_INT_MAX );

        add_action( 'widgets_init', function() {
            register_widget( 'GMCE\Output\BoxWidget' );
        } );

        add_action( 'pending_to_publish', function( $post ) use($brain) {
            $votes = $brain->get( 'votes' );
            $info = $votes->votes_info( $post );
            if ( isset( $info[ 'approvers' ] ) && ! empty( $info[ 'approvers' ] ) ) {
                $votes->confirm_users_vote( array_keys( $info[ 'approvers' ] ), 'up', $post );
            }
            if ( isset( $info[ 'denyers' ] ) && ! empty( $info[ 'denyers' ] ) ) {
                $votes->confirm_users_vote( array_keys( $info[ 'denyers' ] ), 'down', $post );
            }
            update_post_meta( $post->ID, '__gmce_published', 1 );
        } );
        $brain->get( 'cron' )->setup();
    }

    public function getBindings( \Brain\Container $brain ) {

        $brain[ 'settings' ] = function() {
            $s = new SettingsGMCE;
            $s->setup();
            return $s;
        };

        $brain[ 'user' ] = function( $c ) {
            $u = new User( $c[ 'settings' ] );
            $u->setup();
            return $u;
        };

        $brain[ 'reasons' ] = function( $c ) {
            return new Reasons( $c[ 'settings' ] );
        };

        $brain[ 'thumb_votes' ] = function( $c ) {
            return new ThumbVotes( $c[ 'settings' ], $c[ 'user' ] );
        };

        $brain[ 'revision' ] = function( $c ) {
            return new Revision( $c[ 'settings' ], $c[ 'user' ], $c[ 'reasons' ], $c[ 'thumb_votes' ] );
        };

        $brain[ 'revisions' ] = function( $c ) {
            return new Revisions( $c[ 'settings' ], $c[ 'user' ], $c[ 'revision' ], $c[ 'reasons' ] );
        };

        $brain[ 'votes' ] = function( $c ) {
            return new Votes( $c[ 'settings' ], $c[ 'user' ], $c[ 'revisions' ], $c[ 'reasons' ] );
        };

        $brain[ 'twig_loader' ] = function($c) {
            $views = apply_filters( 'gmce_views_path', $c[ 'path' ] . 'src/Output/Views' );
            return new \Twig_Loader_Filesystem( $views );
        };

        $brain[ 'twig' ] = function($c) {
            $path = $c[ 'path' ] . 'src/Output/Views/cache';
            $cache_path = defined( 'WP_DEBUG' ) && WP_DEBUG ? FALSE : $path;
            $cache = apply_filters( 'gmce_twig_cache', $cache_path );
            $te = new \Twig_Environment( $c[ 'twig_loader' ], [ 'cache' => $cache ] );
            return $te;
        };

        $brain[ 'view_provider' ] = function($c) {
            $t = $c[ 'twig' ];
            $vp = new Output\ViewProvider( $t );
            $vp->setup();
            $t->registerUndefinedFunctionCallback( [ $vp, 'twigFunction' ] );
            return $vp;
        };

        $brain[ 'frontend' ] = function($c) {
            return new Output\Frontend( $c[ 'view_provider' ] );
        };

        $brain[ 'backend' ] = function($c) {
            return new Output\Backend( $c[ 'view_provider' ] );
        };

        $brain[ 'ajax_proxy' ] = function($c) {
            return new AjaxProxy();
        };

        $brain[ 'community_editor' ] = function($c) {
            $ce = new CommunityEditor( $c[ 'backend' ], $c[ 'frontend' ], $c[ 'ajax_proxy' ] );
            $ce->setup();
            return $ce;
        };

        $brain[ 'api' ] = function($c) {
            return new Api;
        };

        $brain[ 'reason_templater' ] = function( $c ) {
            return new ReasonsTemplater( $c[ 'settings' ], $c[ 'user' ] );
        };

        $brain[ 'cron' ] = function( $c ) {
            return new Cron;
        };

        $brain[ 'auto_publisher' ] = function($c ) {
            return new AutoPublisher( $c[ 'settings' ] );
        };
    }

    public function getPath() {
        return dirname( dirname( __FILE__ ) );
    }

}