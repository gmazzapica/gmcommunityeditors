<?php

namespace GMCE\Output;

abstract class ViewModel {

    private $t;

    protected $view;

    public function __construct( \Twig_Environment $t ) {
        $this->t = $t;
    }

    public function set_template( $view ) {
        $i = array_search( $this, ViewProvider::$viemodels, TRUE );
        if ( ViewProvider::$viemodels[ $i ] !== $this || $view !== $i ) return;
        $this->view = new View( $view . '.twig' );
    }

    public function render( $data = NULL ) {
        if ( is_null( $this->view ) ) return;
        $this->set_data( $data );
        return $this->t->render( $this->view->get_template(), $this->view->get_data() );
    }

    abstract function set_data( $data = NULL );
}