<?php namespace GMCE\Output;

use Brain\Container as Brain;

/**
 * Backend Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Backend extends Output {

    protected $revision;

    function actions() {
        if ( Brain::instance()->get( 'revisions' )->is_cpt_public() ) {
            add_action( 'edit_form_after_title', [ $this, 'rev_post_info' ] );
        }
        add_action( 'load-edit.php', [$this, 'list_ui' ] );
        add_action( 'load-post.php', [$this, 'post_ui' ] );
        if ( $this->user->is_admin ) {
            add_action( 'personal_options', [$this, 'profile_ban_field' ] );
            add_action( 'edit_user_profile_update', [$this, 'profile_ban_field_save' ] );
        }
    }

    function assets() {
        if ( empty( $this->post ) || ! $this->user->is_admin_or_author ) return;
        parent::assets();
        $use_min = $this->settings->get( 'use_minified_assets', FALSE ) ? '.min' : '';
        $url = Brain::instance()->get( 'url' ) . "assets/js/gmce_backend{$use_min}.js";
        $path = Brain::instance()->get( 'path' ) . "assets/js/gmce_backend{$use_min}.js";
        wp_enqueue_script( 'gmce_backend', $url, 'gmce', filemtime( $path ), TRUE );
    }

    protected function check( $post = null ) {
        if ( ! $post instanceof \WP_Post ) {
            $type = gmce_get_get( 'post_type' ) ? : 'post';
        } else {
            $type = $post->post_type;
        }
        return ( in_array( $type, (array) $this->settings->get( 'post_types', ['post' ] ) ) );
    }

    function post_ui() {
        $this->setup_post();
        if ( empty( $this->post ) ) return;
        if ( $this->user->is_admin_or_author && $this->user->can_box ) {
            add_action( 'admin_enqueue_scripts', [$this, 'assets' ] );
            $this->vote_box();
        }
        if ( $this->user->is_admin ) $this->admin_post_ui();
    }

    function admin_post_ui() {
        $status = $this->post->post_status;
        if ( $status !== 'pending' && $status !== 'draft' ) return;
        if ( $status === 'draft' ) {
            $has = get_post_meta( $this->post->ID, '__gmce_votes', TRUE );
            if ( empty( $has ) ) return;
        }
        $scr = get_current_screen();
        add_meta_box(
            'gmce_admin_box', $this->msg[ 'backend' ][ 'adminbox_title' ], [$this, 'admin_box' ], $scr->post_type, 'advanced', 'high'
        );
    }

    function admin_box() {
        do_action( 'gmce_before_adminbox' );
        $data = apply_filters( 'gmce_adminbox_data', [
            'post'          => $this->post,
            'was_published' => get_post_meta( $this->post->ID, '__gmce_published', TRUE ),
            'info'          => $this->votes->votes_info( $this->post ),
            'strings'       => $this->msg,
            'limit'         => $this->settings->get( 'vote_limit', 10 )
            ]
        );
        return $this->print_view( 'AdminBox', 'adminbox', $data );
    }

    function vote_box() {
        if ( ! $this->user->is_admin_or_author ) return;
        $scr = get_current_screen();
        add_meta_box(
            'gmce_admin_votebox', $this->msg[ 'community_revisions' ], [$this, 'vote_box_print' ], $scr->post_type, 'side', 'high'
        );
    }

    function vote_box_print() {
        if ( $this->user->is_admin_or_author ) {
            do_action( 'gmce_vote_box', $this->post );
        }
    }

    function list_ui() {
        $this->assets();
        if ( ! $this->check() ) return;
        $status = gmce_get_get( 'post_status' );
        if ( $status === 'pending' ) {
            $this->pending_list_ui();
        } else {
            $this->non_pending_list_ui();
        }
    }

    function profile_ban_field( \WP_User $user ) {
        $current = wp_get_current_user();
        if ( ! is_admin() || $user->ID === $current->ID ) return;
        if ( ! user_can( $current, 'edit_users' ) ) return;
        $ce_role = $this->settings->get( 'role_name', 'community_editor' );
        if ( user_can( $user, $ce_role ) ) {
            $banned_meta = get_user_meta( $user->ID, 'gmce_thumb_banned', TRUE );
            $banned = ! empty( $banned_meta );
            $view_data = [
                'options_title' => __( 'Community Editor', 'gmce' ),
                'thumb_votes'   => __( 'Thumb Votes', 'gmce' ),
                'checked'       => checked( TRUE, $banned, FALSE ),
                'label'         => __( 'Ban from revisions thumb votes', 'gmce' )
            ];
            if ( (int) $banned_meta > 1000000000 ) {
                $banned_meta += get_option( 'gmt_offset' ) * HOUR_IN_SECONDS;
                $format = get_option( 'date_format' ) . ' @ ' . get_option( 'time_format' );
                $auto = __( 'auto banned on %s', 'gmce' );
                $view_data[ 'label' ] .= ' (' . sprintf( $auto, date( $format, $banned_meta ) ) . ')';
            } elseif ( $banned ) {
                $view_data[ 'label' ] .= ' (' . __( 'manually banned', 'gmce' ) . ')';
            }
            echo Brain::instance()->get( 'twig' )->render( 'ProfileFields.twig', $view_data );
        }
    }

    function profile_ban_field_save( $userid ) {
        if ( ! current_user_can( 'edit_users' ) ) return;
        $ce_role = $this->settings->get( 'role_name', 'community_editor' );
        if ( ! user_can( $userid, $ce_role ) ) return;
        $ban = filter_input( INPUT_POST, 'thumb_vote_ban', FILTER_SANITIZE_NUMBER_INT );
        if ( (int) $ban > 0 ) {
            update_user_meta( $userid, 'gmce_thumb_banned', 1 );
        } elseif ( get_user_meta( $userid, 'gmce_thumb_banned', TRUE ) ) {
            delete_user_meta( $userid, 'gmce_thumb_banned' );
        }
    }

    protected function pending_list_ui() {
        if ( ! $this->user->is_admin || $this->settings->get( 'pending_ui_for_admins', TRUE ) ) {
            $types = $this->settings->get( 'post_types', ['post' ] );
            foreach ( $types as $type ) {
                if ( $type == 'post' || $type == 'page' ) $type .= 's';
                add_filter( 'manage_' . $type . '_columns', [$this, 'cols' ] );
                add_action( 'manage_' . $type . '_custom_column', [$this, 'col' ], 10, 2 );
            }
        }
    }

    protected function non_pending_list_ui() {
        if ( ! $this->user->is_admin || $this->settings->get( 'pending_ui_for_admins', TRUE ) ) {
            add_filter( 'post_row_actions', [$this, 'no_pending_info' ], 20, 2 );
            add_filter( 'page_row_actions', [$this, 'no_pending_info' ], 20, 2 );
        }
    }

    function no_pending_info( $actions, $post ) {
        if ( ! $this->check( $post ) ) return $actions;
        $is_author = ( (int) $this->user->ID === (int) $post->post_author);
        if ( $this->user->is_ce || $this->user->is_admin || $is_author ) {
            if ( $post->post_status === 'draft' && ! $is_author ) return $actions;
            $post_info = $this->votes->votes_info( $post );
            if ( $post->post_status === 'draft' && $post_info[ 'total' ] < 1 ) return $actions;
            $promote_title = esc_html( sprintf( $this->msg[ 'approved_num' ], $post_info[ 'approved' ] ) );
            $demote_title = esc_html( sprintf( $this->msg[ 'unfixed_num' ], $post_info[ 'unfixed' ] ) );
            $format = '<span style="color:#090">%s</span>&nbsp;/&nbsp;'
                . '<span style = "color:#900">%s</span>';
            $out = sprintf( $format, $promote_title, $demote_title );
            if ( $post_info[ 'locked' ] ) {
                $out .= '&nbsp;<span>&mdash;&nbsp;' . $this->msg[ 'locked' ] . '<span>';
            }
            $actions[ 'gmce_stats' ] = $out;
        }
        return $actions;
    }

    function cols( $cols ) {
        if ( ! $this->check() ) return;
        $new = ['cb' => $cols[ 'cb' ], 'title' => $cols[ 'title' ] ];
        unset( $cols[ 'cb' ], $cols[ 'title' ] );
        $new[ 'gmce_stats' ] = $this->msg[ 'list_arrows' ];
        if ( $this->settings->get( 'show_score_col', TRUE ) ) {
            $new[ 'gmce_score' ] = $this->msg[ 'score_label' ];
        }
        return array_merge( $new, $cols );
    }

    function col( $col, $postid ) {
        if ( $col !== 'gmce_stats' && $col !== 'gmce_score' ) return;
        $post = get_post( $postid );
        if ( ! $this->check( $post ) ) return;
        if ( $post->post_status !== 'pending' && $post->post_status !== 'draft' ) {
            $this->col_emty();
        } else {
            $this->col_pending( $col, $post );
        }
    }

    protected function col_pending( $col, $post ) {
        $author = ( (int) $post->post_author === (int) $this->user->ID );
        $allowed = ( $this->user->is_admin || $this->user->is_ce );
        if ( ! $author && ! $allowed ) return $this->col_emty();
        $post_info = $this->votes->votes_info( $post );
        if ( $post->post_status === 'draft' && $post_info[ 'total' ] === 0 ) {
            return $this->col_emty();
        }
        if ( $col === 'gmce_stats' ) {
            $this->col_pending_stats( $post, $post_info, $author );
        } elseif ( $col === 'gmce_score' ) {
            $this->col_pending_score( $post_info );
        }
    }

    protected function col_pending_stats( $post, $post_info, $author ) {
        $preview_url = add_query_arg( ['preview' => 'true' ], get_permalink( $post->ID ) );
        $format = '<span style="color:#090">%d</span>&nbsp;/&nbsp;'
            . '<span style="color:#900">%d</span>&nbsp;<b>';
        $linkformat = '<a href="%s" target="_blank">%s</a>';
        $label_key = $author || $post_info[ 'locked' ] ? 'check_approve_and_revise' : 'approve_or_revise';
        $link = sprintf( $linkformat, $preview_url, $this->msg[ $label_key ] );
        $format .= $link;
        if ( $post_info[ 'locked' ] ) {
            $format .= " ({$this->msg[ 'locked' ]})";
        }
        $format .= '</b>';
        printf( $format, $post_info[ 'approved' ], $post_info[ 'unfixed' ] );
    }

    protected function col_pending_score( $post_info ) {
        $score = (int) $post_info[ 'score' ];
        $limit = (int) $this->settings->get( 'vote_limit', 10 );
        $color = $score === $limit ? 'style = "color:#090"' : ' style="color:#900"';
        printf( '<strong %s>%d</strong> ', $color, $score );
    }

    protected function col_emty() {
        echo '&mdash;';
    }

    function rev_post_info( $post ) {
        if ( ! Brain::instance()->get( 'revisions' )->is_cpt_public() ) return;
        if ( Brain::instance()->get( 'cpt' ) === $post->post_type ) {

        }
    }

}