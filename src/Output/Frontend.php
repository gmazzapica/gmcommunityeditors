<?php

namespace GMCE\Output;

/**
 * Frontend Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Frontend extends Output {

    function setup() {
        parent::setup();
        $this->setup_post();
    }

    function actions() {
        add_action( 'wp_enqueue_scripts', [ $this, 'assets' ] );
        add_action( 'gmce_vote_box', [ $this, 'create_box' ] );
    }

    function create_box() {
        $votes = $this->votes->get_post_votes( $this->post );
        if ( $this->can_box( $votes ) === FALSE ) return;
        do_action( 'gmce_before_votebox' );
        if ( $this->user->is_author ) return $this->create_revisions_box();
        $this->votes->lock_or_not( $this->post, $votes );
        $info = $this->votes->votes_info( $this->post );
        $data = apply_filters( 'gmce_votebox_data', [
            'votes'         => $votes,
            'votes_info'    => $info,
            'post'          => $this->post,
            'help'          => $this->create_score_help( $this->post, $info ),
            'user'          => $this->user,
            'strings'       => $this->msg,
            'user_post_rev' => $this->revisions->user_post_rev( $this->user, $this->post ),
            'settings'      => [
                'undo_vote'     => $this->settings->get( 'allow_undo_vote', TRUE ),
                'approve_label' => $this->settings->get( 'approve_btn_label', $this->msg[ 'approve_label' ] ),
                'deny_label'    => $this->settings->get( 'deny_btn_label', $this->msg[ 'deny_label' ] ),
                'undo_label'    => $this->settings->get( 'undo_rev_btn_label', $this->msg[ 'undo_rev_btn_label' ] ),
                'vote_limit'    => $this->settings->get( 'vote_limit', 10 ),
                'reasons'       => $this->settings->get( 'reasons', [ ] )
            ] ]
        );
        return $this->print_view( 'Box', 'votebox', $data );
    }

    function create_revisions_box() {
        $votes = $this->votes->get_post_votes( $this->post );
        if ( $this->can_box( $votes ) === FALSE ) return;
        do_action( 'gmce_before_revisionbox' );
        $revisions = $this->revisions->post_revs( $this->post, 'publish', FALSE );
        $this->votes->lock_or_not( $this->post );
        $info = $this->votes->votes_info( $this->post );
        $data = apply_filters( 'gmce_revisionbox_data', [
            'revisions'  => $revisions,
            'strings'    => $this->msg,
            'votes_info' => $info,
            'help'       => $this->create_score_help( $this->post, $info ),
            'reasons'    => $this->settings->get( 'reasons', [ ] ),
            'post'       => $this->post,
            'vote_limit' => $this->settings->get( 'vote_limit', 10 ),
            'user'       => $this->user
            ] );
        return $this->print_view( 'Revisions', 'revisionbox', $data );
    }

    function comments_popup( $revision ) {
        if ( empty( $this->post ) || ! $this->user->can_box ) return;
        do_action( 'gmce_before_commentpopup' );
        $html = $this->settings->get( 'post_html', [ ] );
        $content = wpautop( wptexturize( $this->post->post_content ) );
        $this->post->post_content = wp_kses( nl2br( $content ), $html );
        $data = apply_filters( 'gmce_commentpopup_data', [
            'strings'        => $this->msg,
            'revision'       => $revision,
            'post'           => $this->post,
            'user'           => $this->user,
            'order'          => $this->settings->get( 'comment_default_order', 'DESC' ),
            'allow_original' => $this->settings->get( 'show_original_in_comments', TRUE )
            ] );
        return $this->print_view( 'Comments', 'commentpopup', $data );
    }

    function thumb_votes_popup( $revision ) {
        if ( ! $this->user->is_admin_or( [ $revision->post_author, $this->post->post_author ] ) ) {
            return;
        }
        do_action( 'gmce_before_thumb_votes_popup' );
        $data = apply_filters( 'gmce_commentpopup_data', [
            'strings'  => $this->msg,
            'revision' => $revision,
            'user'     => $this->user,
            'post'     => $this->post
            ] );
        return $this->print_view( 'ThumbVotes', 'thumbvotes', $data );
    }

    function create_score_help( $post = NULL, $info = NULL ) {
        if ( empty( $post ) ) $post = $this->post;
        if ( empty( $info ) ) $info = $this->votes->votes_info( $post );
        $msg = $this->msg;
        $limit = $this->settings->get( 'vote_limit', 10 );
        if ( $limit === $info[ 'score' ] ) {
            return $msg[ 'help_score_good' ];
        } else {
            $help = sprintf( $msg[ 'help_score_bad' ], $limit );
            if ( $info[ 'unfixed' ] )
                    $help .= PHP_EOL . sprintf( $msg[ 'help_score_fix' ], $info[ 'unfixed' ] );
            $need = $limit - ($info[ 'approved' ] + $info[ 'denied' ]);
            if ( $need > 0 ) $help .= PHP_EOL . sprintf( $msg[ 'help_score_early' ], $need );
            return $help;
        }
    }

    function can_box( $votes = [ ] ) {
        if ( empty( $this->post ) || ! $this->user->can_box ) return FALSE;
        if ( $this->post->post_status !== 'draft' && $this->post->post_status !== 'pending' )
                return FALSE;
        if ( $this->post->post_status === 'draft' && empty( $votes ) ) return FALSE;
    }

}