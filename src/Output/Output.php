<?php namespace GMCE\Output;

use Brain\Container as Brain;

/**
 * Output Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
abstract class Output extends \GMCE\Base\Providerable {

    protected $view;
    protected $post;

    function __construct( ViewProvider $v ) {
        $this->view = $v;
    }

    function setup() {
        $this->provider();
        $this->actions();
    }

    function setup_post() {
        if ( ! $this->user->allowed_post instanceof \WP_Post ) return;
        $allowed = $this->settings->get( 'post_types', [ 'post' ] );
        if ( ! in_array( $this->user->allowed_post->post_type, $allowed ) ) return;
        $this->post = $this->user->allowed_post;
    }

    function get_post() {
        return $this->post;
    }

    function print_view( $viewmodel, $id, $data ) {
        $renderer = $this->view->get( $viewmodel );
        $output = apply_filters( "gmce_{$id}_html", $renderer->render( $data ) );
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) return $output;
        echo $output;
    }

    function scripts() {
        $use_min = $this->settings->get( 'use_minified_assets', TRUE ) ? '.min' : '';
        $jsurl = Brain::instance()->get( 'url' ) . 'assets/js';
        $url = "{$jsurl}/gmce{$use_min}.js";
        $path = Brain::instance()->get( 'path' ) . "assets/js/gmce{$use_min}.js";
        wp_register_script( 'tinysort', $jsurl . '/tinysort.js', 'jquery', NULL );
        $deps = [ 'jquery', 'thickbox', 'tinysort', 'easyajax' ];
        wp_enqueue_script( 'gmce', $url, $deps, filemtime( $path ), TRUE );
    }

    function styles() {
        $embed = $this->settings->get( 'use_embedded_style', TRUE );
        if ( ! $embed ) return;
        $use_min = $this->settings->get( 'use_minified_assets' ) ? '-min' : '';
        $url = Brain::instance()->get( 'url' ) . "assets/css/gmce-box{$use_min}.css";
        $style = $this->settings->get( 'box_css', $url );
        wp_enqueue_style( 'gmce-box', $style, [ 'thickbox' ], NULL );
    }

    function scripts_data() {
        $approve_label = $this->settings->get( 'approve_btn_label', $this->msg[ 'approve_label' ] );
        $deny_label = $this->settings->get( 'deny_btn_label', $this->msg[ 'deny_label' ] );
        $data = $this->msg[ 'frontend_localize' ];
        $data[ 'thumbs_msg' ] = $this->msg[ 'thumbs' ];
        $data[ 'approve_label' ] = $approve_label;
        $data[ 'deny_label' ] = $deny_label;
        $data[ 'allow_double' ] = $this->settings->get( 'allow_undo_vote', TRUE );
        wp_localize_script( 'gmce', 'GMCommunityEditorBoxData', $data );
    }

    function assets() {
        if ( empty( $this->post ) || ! $this->user->can_box ) return;
        $this->styles();
        $this->scripts();
        $this->scripts_data();
    }

    abstract function actions();
}