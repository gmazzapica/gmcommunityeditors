<?php

namespace GMCE\Output\ViewModels;

class Box extends \GMCE\Output\ViewModel {

    function set_data( $data = NULL ) {
        $this->user = $data['user'];
        $this->view->is_loked = $this->check_locked( $data );
        $this->view->show_rev_link = ( $this->user->is_admin && $this->view->is_loked );
        $this->setup_title( $data['votes_info'], $data['strings'] );
        $this->setup_buttons( $data['strings'], $data['settings'] );
        $this->setup_approvers( $data['votes_info'], $data['strings'] );
        $this->setup_deniers( $data['votes_info'], $data['strings'] );
        $this->setup_reasons( $data['votes_info'], $data['settings'] );
        $this->setup_strings( $data['strings'] );
        $this->setup_vars( $data );
    }

    protected function setup_title( $info, $strings ) {
        $this->view->score = $info['score'];
        $this->view->score_label = $strings['score_label'];
        $this->view->locked = $strings['locked'];
    }

    protected function check_locked( $data ) {
        if ( $data['votes_info']['locked'] ) {
            $this->view->is_meta_locked = TRUE;
            $this->view->can_deny = FALSE;
            $this->view->can_approve = FALSE;
            $this->check_locked_had( $data );
            return TRUE;
        }
        $strings = $data['strings'];
        if ( $this->check_locked_author( $strings ) ) return TRUE;
        if ( $this->check_locked_triple( $data, $strings ) ) return TRUE;
        if ( $this->check_locked_had( $data ) ) return TRUE;
    }

    protected function check_locked_author( $strings ) {
        if ( $this->user->is_author ) {
            $this->view->can_deny = FALSE;
            $this->view->can_approve = FALSE;
            $this->view->feedback = esc_html( $strings['ajax_vote_reponses']['author'] );
            return TRUE;
        }
    }

    protected function check_locked_triple( $data, $msg ) {
        if ( $this->user->had_voted( $data['votes'], 'triple' ) ) {
            $rev = $data['user_post_rev'];
            $fixed = '';
            if ( $rev instanceof \GMCE\Revision && $rev->fixed ) {
                $fixed = '<p class="marked_fixed">' . esc_html( $msg['marked_fixed'] ) . '</p>';
            }
            $this->view->feedback = esc_html( $msg['ajax_vote_reponses']['triple'] ) . $fixed;
            $this->view->can_deny = FALSE;
            $this->view->can_approve = FALSE;
            return TRUE;
        }
    }

    protected function check_locked_had( $data ) {
        $undo = $data['settings']['undo_vote'];
        $user_rev = $data['user_post_rev'];
        $this->view->can_deny = ! ( $user_rev instanceof \GMCE\Revision );
        $this->view->can_approve = ! in_array( $this->user->ID, $data['votes']['up'] );
        if ( ! $this->view->can_deny && ! $this->view->can_approve ) $undo = FALSE;
        if ( ! $this->view->can_deny ) return $this->cant_deny( $user_rev, $data['strings'] );
        if ( ! $this->view->can_approve ) return $this->cant_approve( $undo, $data['strings'] );
    }

    protected function cant_approve( $undo, $msg ) {
        $this->view->feedback = $msg['already_voted_up'];
        return ! $undo;
    }

    protected function cant_deny( $rev, $msg ) {
        $time = gmce_human_post_time( $rev->ID );
        $already = sprintf( '<p>' . esc_html( $msg['already_revised'] ) . '</p>', $time );
        $format = '<p align="left" class="already_revised_msg">' . $msg['already_revised_msg'] . '</p>';
        $title = sprintf( '&quot;%s&quot;', wp_filter_nohtml_kses( $rev->reason ) );
        $content = sprintf( '&quot;%s&quot;', wp_filter_nohtml_kses( $rev->summary ) );
        $fixed = ( $rev->fixed ) ? '<p class="marked_fixed">' . $msg['marked_fixed'] . '</p>' : '';
        $this->view->feedback = $already . sprintf( $format, $title, $content ) . $fixed;
        return TRUE;
    }

    protected function setup_buttons( $strings, $settings ) {
        $this->view->approve_btn = $this->setup_approve_btn( $strings, $settings );
        $this->view->deny_btn = $this->setup_deny_btn( $strings, $settings );
    }

    protected function setup_approve_btn( $strings, $settings ) {
        $approve = $settings['approve_label'] ? : $strings['approve_btn_label'];
        $undo = $settings['undo_label'] ? : $strings['undo_rev_btn_label'];
        return [
            'disabled' => $this->view->can_approve ? '' : ' disabled =\'disabled\'',
            'class'    => $this->view->can_approve ? 'gmce_approve_btn' : 'gmce_approve_btn disabled',
            'label'    => $this->view->can_deny ? $approve : $undo
        ];
    }

    protected function setup_deny_btn( $strings, $settings ) {
        return [
            'disabled' => $this->view->can_deny ? '' : ' disabled =\'disabled\'',
            'class'    => $this->view->can_deny ? 'gmce_deny_btn' : 'gmce_deny_btn disabled',
            'label'    => $settings['deny_label'] ? : $strings['deny_btn_label']
        ];
    }

    protected function setup_approvers( $info, $str ) {
        $this->view->approve_num = count( $info['approvers'] );
        if ( empty( $info['approvers'] ) ) {
            $this->view->approvers = esc_html( $str['no_approvers'] );
            return;
        }
        $approvers = $info['approvers'];
        $f = esc_html( $str['already_approved'] );
        $this->view->is_admin = $this->user->is_admin;
        if ( ! $this->user->is_admin ) {
            $this->view->approvers = sprintf( $f, implode( ', ', $approvers ) ) . '.';
        } else {
            $this->setup_approvers_admin( $approvers, $str );
        }
    }

    protected function setup_approvers_admin( $approvers, $str ) {
        $showf = '<a href="#" id="gmce_approvers_show_details">(%s)</a>';
        $show = sprintf( $showf, \esc_html( $str['frontend_localize']['show'] ) );
        $f = esc_html( $str['already_approved'] ) . ' ' . $show;
        $count = count( $approvers );
        $u = translate_nooped_plural( $str['user_s'], $count, 'gmce' );
        $by = sprintf( '%d ' . $u, $count );
        $this->view->approvers_label = trim( sprintf( $f, $by ) );
        $this->view->delete_approve_label = $str['frontend_localize']['delete_approve_label'];
        $this->view->delete_approve = $str['frontend_localize']['delete_approve'];
        $this->view->approvers = $approvers;
    }

    protected function setup_deniers( $info, $strings ) {
        $this->view->deny_num = (int) $info['fixed'] + (int) $info['unfixed'];
        $this->view->comm_revs = (int) $info['denied'];
        if ( empty( $info['denyers'] ) ) {
            $this->view->denyers = esc_html( $strings['no_denyers'] );
            return;
        }
        $denyers = implode( ', ', $info['denyers'] );
        $this->view->denyers = sprintf( esc_html( $strings['already_denied'] ), $denyers );
    }

    protected function setup_reasons( $info, $settings ) {
        $this->view->reasons = [ ];
        $reasons = $settings['reasons'];
        if ( empty( $reasons ) ) return;
        foreach ( array_keys( $reasons ) as $reason ) {
            $this->view->reasons[] = [
                'sanitized' => sanitize_title_with_dashes( $reason ),
                'slug'      => esc_attr( $reason ),
                'name'      => esc_html( $reasons[$reason][0] ),
                'count'     => isset( $info['reasons'][$reason] ) ? (int) $info['reasons'][$reason] : 0,
                'desc'      => esc_html( $reasons[$reason][1] )
            ];
        }
    }

    protected function setup_strings( $strings ) {
        foreach ( [ 'locked', 'yes', 'no' ] as $k ) {
            $this->view->$k = $strings[$k];
        }
        $this->view->comment_placeholder = $strings['add_deny_comment'];
        $this->view->rev_link = $strings['show_revision'];
    }

    protected function setup_vars( $data ) {
        $this->view->post_id = $data['post']->ID;
        $this->view->process_now = $data['votes_info']['approved'] + $data['votes_info']['denied'];
        $fin = $data['votes_info']['final'];
        $this->view->final = $fin;
        $format = translate_nooped_plural( $data['strings']['has_final'], $fin, 'gmce' );
        $this->view->has_final = sprintf( $format, $fin );
        $this->view->process_limit = $data['settings']['vote_limit'];
        $this->view->process_title = $data['strings']['process_title'];
        $this->view->help_score = $data['help'];
    }

}