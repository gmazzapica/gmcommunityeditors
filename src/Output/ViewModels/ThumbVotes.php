<?php

namespace GMCE\Output\ViewModels;

class ThumbVotes extends \GMCE\Output\ViewModel {

    public function set_data( $data = NULL ) {
        $this->view->user = $data[ 'user' ];
        $this->view->strings = $data[ 'strings' ];
        $this->view->revision = $data[ 'revision' ];
        $this->view->post = apply_filters( 'the_title', $data[ 'post' ]->post_title );
        $author_f = '<a href="%s" target="_blank">%s</a>';
        $r_author = new \WP_User( $data[ 'revision' ]->post_author );
        $this->view->rev_author = sprintf(
            $author_f, get_author_posts_url( $r_author->ID ), $r_author->display_name
        );
        $p_author = new \WP_User( $data[ 'post' ]->post_author );
        $this->view->post_author = sprintf(
            $author_f, get_author_posts_url( $p_author->ID ), $p_author->display_name
        );
    }

}