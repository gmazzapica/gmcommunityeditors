<?php

namespace GMCE\Output\ViewModels;

class Comments extends \GMCE\Output\ViewModel {

    function set_data( $data = null ) {
        $this->view->user = $data['user'];
        $this->view->strings = $data['strings'];
        $this->view->revision = $data['revision'];
        $this->view->comments = $data['revision']->comments;
        $this->view->allow_original = $data['allow_original'];
        $order = strtoupper( $data['order'] ) !== 'DESC' ? 'dateasc' : 'datedesc';
        $this->view->order = $order;
        $this->view->order_default = $data['strings']['frontend_localize']['orderby_' . $order];
        $this->set_post( $data['post'] );
        $this->set_strings( $data );
    }

    protected function set_strings( $data ) {
        $this->view->fixed = strtoupper( $data['strings']['frontend_localize']['fixed'] );
        $this->view->delete = $data['strings']['frontend_localize']['delete'];
        $this->view->deleted = $data['strings']['frontend_localize']['deleted'];
        $strings = [
            'revision_colon', 'add_comments', 'nocomments', 'bethefirst', 'commenthere',
            'comment_close', 'submit_comment', 'show_original', 'original_by', 'comments_label'
        ];
        foreach ( $strings as $string ) {
            $this->view->$string = esc_html( $data['strings'][$string] );
        }
    }

    protected function set_post( $post ) {
        $post->title = $post->post_title;
        $post->content = $post->post_content;
        $post->author = get_the_author_meta( 'display_name', $post->post_author );
        $this->view->post = $post;
    }

}