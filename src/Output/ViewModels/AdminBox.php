<?php

namespace GMCE\Output\ViewModels;

class AdminBox extends \GMCE\Output\ViewModel {

    function set_data( $data = null ) {
        $this->view->strings = $data[ 'strings' ];
        $this->view->info = $data[ 'info' ];
        $this->view->post = $data[ 'post' ];
        $this->view->was_published = $data[ 'was_published' ];
        $f = $data[ 'strings' ][ 'backend' ][ 'process' ];
        $now = $data[ 'info' ][ 'approved' ] + $data[ 'info' ][ 'denied' ];
        $this->view->process_progress = \sprintf( $f, $now, $data[ 'limit' ] );
    }

}