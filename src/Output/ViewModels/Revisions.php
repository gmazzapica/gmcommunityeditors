<?php

namespace GMCE\Output\ViewModels;

class Revisions extends \GMCE\Output\ViewModel {

    function set_data( $data = NULL ) {
        if ( \is_array( $data ) ) {
            $this->setup_vars( $data );
            $this->setup_info( $data[ 'votes_info' ] );
            $this->set_approved( $data[ 'votes_info' ], $data[ 'strings' ] );
            $this->setup_reasons( $data[ 'reasons' ], $data[ 'votes_info' ], $data[ 'strings' ] );
            $this->setup_strings( $data[ 'strings' ] );
        }
    }

    protected function setup_vars( $data ) {
        $this->view->user = $data[ 'user' ];
        $this->view->revisions = $data[ 'revisions' ];
        $this->view->post = $data[ 'post' ];
        $this->view->process_limit = $data[ 'vote_limit' ];
        $this->view->help_score = $data[ 'help' ];
    }

    protected function setup_info( $info ) {
        $this->view->process_now = (int) $info[ 'approved' ] + (int) $info[ 'denied' ];
        $this->view->is_locked = $info[ 'locked' ];
        $this->view->score = $info[ 'score' ];
        $this->view->unfixed_count = $info[ 'unfixed' ] ? : '0';
    }

    protected function set_approved( $info, $strings ) {
        $this->view->approved = (int) $info[ 'approved' ] ? : FALSE;
        $format = \translate_nooped_plural(
            $strings[ 'approved_times' ], $this->view->approved, 'GMCommunityEditor'
        );
        $this->view->approved_times = \sprintf( $format, $this->view->approved );
    }

    protected function setup_reasons( $reasons, $info, $msg ) {
        $this->view->reasons = [ ];
        $all_reasons = array_merge( $info[ 'reasons' ], $info[ 'final_reasons' ] );
        foreach ( $all_reasons as $slug => $count ) {
            $this->view->reasons[] = [
                'name'  => $slug != '_final_revision_' ? $reasons[ $slug ][ 0 ] : $msg[ 'final_rev_title' ],
                'count' => (int) $count
            ];
        }
    }

    protected function setup_strings( $strings ) {
        foreach ( [
        'fixed', 'close_all_rev', 'close_fixed_rev', 'expand_all_rev', 'close_rev', 'delete_rev',
        'delete_rev_label'
        ] as $k ) {
            $this->view->$k = $strings[ 'frontend_localize' ][ $k ];
        }
        foreach ( [
        'community_revisions', 'rev_summary', 'show_vote_box', 'show_add_comments', 'add_comments',
        'locked', 'score_label', 'process_title', 'no_revisions', 'total', 'unfixed', 'unfixed_label',
        'revisions_label', 'no_approvers', 'final_rev_title', 'final_revision', 'editors_label',
        'deny_btn_label', 'add_final_rev_comment', 'thumbs', 'yes', 'no'
        ] as $k ) {
            $this->view->$k = $strings[ $k ];
        }
    }

}