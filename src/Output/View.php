<?php

namespace GMCE\Output;

class View {

    private $template;

    function __construct( $tpl = null ) {
        $this->template = $tpl;
    }

    function get_template() {
        return $this->template;
    }

    function get_data() {
        return \get_object_vars( $this );
    }

}