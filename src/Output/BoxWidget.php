<?php

namespace GMCE\Output;

class BoxWidget extends \WP_Widget {

    /**
     * Construct the widget
     *
     * @access public
     * @return null
     *
     */
    function __construct() {
        $widget_ops = [
            'description' => __( 'Community Editor widget, only for preview or pending posts.', 'gmce' )
        ];
        parent::__construct( 'GMCEBoxWidget', 'GM Community Editor', $widget_ops );
    }

    /**
     * Print the widget on frontend
     *
     * @access public
     * @return null
     *
     */
    function widget( $args, $instance ) {
        if ( ! is_preview() || ! is_singular() ) return;
        global $post;
        if ( empty( $post ) || ! $post instanceof \WP_Post ) return;
        if ( $post->post_status !== 'pending' ) return;
        $user = wp_get_current_user();
        if ( ! $user instanceof \WP_User || empty( $user->ID ) ) return;
        do_action( 'gmce_vote_box', $post );
    }

}