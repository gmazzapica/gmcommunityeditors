<?php

namespace GMCE\Output;

class ViewProvider {

    static $viemodels = [ ];

    protected $t;

    protected $functions = [
        'do_action',
        'apply_filters'
    ];

    function __construct( \Twig_Environment $t ) {
        $this->t = $t;
    }

    function setup() {
        $this->functions = apply_filters( 'gmce_twig_allowed_functions', $this->functions );
    }

    function get( $class ) {
        if ( isset( static::$viemodels[ $class ] ) ) return static::$viemodels[ $class ];
        $full = __NAMESPACE__ . '\\ViewModels\\' . $class;
        if ( ! \class_exists( $full ) ) return;
        static::$viemodels[ $class ] = new $full( $this->t );
        static::$viemodels[ $class ]->set_template( $class );
        return static::$viemodels[ $class ];
    }

    function twigFunction( $name ) {
        if ( function_exists( $name ) && in_array( $name, $this->functions ) ) {
            return new \Twig_SimpleFunction(
                $name, function () use ( $name ) {
                ob_start();
                $return = call_user_func_array( $name, func_get_args() );
                $echo = ob_get_clean();
                return empty( $echo ) ? $return : $echo;
            }, [ 'is_safe' => [ 'all' ] ]
            );
        }
        return FALSE;
    }

}