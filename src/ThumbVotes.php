<?php

namespace GMCE;

/**
 * ThumbVotes Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class ThumbVotes {

    protected static $meta_key = '_gmce_thumb_votes';

    protected $settings;

    protected $user;

    protected $default = [
        'up'    => [ ],
        'down'  => [ ],
        'score' => 0
    ];

    public function __construct( SettingsGMCE $s, User $u ) {
        $this->settings = $s;
        $this->user = $u;
    }

    public function get_votes( $rev = NULL ) {
        $revid = $this->get_rev( $rev );
        if ( ! is_int( $revid ) ) return $this->default;
        $votes = get_post_meta( $revid, static::$meta_key, TRUE ) ? : [ ];
        return array_merge( $this->default, (array) $votes );
    }

    public function get_voters( $rev = NULL, $which = 'up' ) {
        if ( ! in_array( $which, ['both', 'up', 'down' ], TRUE ) ) $which = 'up';
        $votes = $this->get_votes( $rev );
        $voters = [ ];
        if ( $which !== 'down' ) {
            $voters['up'] = ! empty( $votes['up'] ) ? $this->get_voters_info( $votes, 'up' ) : [ ];
        }
        if ( $which !== 'up' ) {
            $voters['down'] = ! empty( $votes['down'] ) ? $this->get_voters_info( $votes, 'down' ) : [ ];
        }
        return $which === 'both' ? $voters : $voters[$which];
    }

    public function get_score( $rev = NULL ) {
        $votes = $this->get_votes( $rev );
        return $votes['score'];
    }

    public function user_voted_up( $rev = NULL, $userid = NULL ) {
        return $this->user_voted( $rev, $userid ) === 1;
    }

    public function user_voted_down( $rev = NULL, $userid = NULL ) {
        return $this->user_voted( $rev, $userid ) === -1;
    }

    public function user_not_voted( $rev = NULL, $userid = NULL ) {
        return $this->user_voted( $rev, $userid ) === FALSE;
    }

    public function user_voted( $rev = NULL, $userid = NULL ) {
        if ( empty( $userid ) ) $userid = $this->user->ID;
        $votes = $this->get_votes( $rev );
        if ( in_array( $userid, $votes['up'] ) ) return 1;
        if ( in_array( $userid, $votes['down'] ) ) return -1;
        return FALSE;
    }

    public function thumb_up( $rev = NULL ) {
        if ( $this->user_voted_up( $rev ) ) {
            return $this->thumb_down( $rev );
        }
        return $this->thumb( $rev, 1 );
    }

    public function thumb_down( $rev = NULL ) {
        if ( $this->user_voted_down( $rev ) ) {
            return $this->thumb_up( $rev );
        }
        return $this->thumb( $rev, -1 );
    }

    public function is_author( $rev = NULL ) {
        $revid = $this->get_rev( $rev );
        if ( ! is_int( $revid ) ) return -1;
        $author = get_post_field( 'post_author', $revid );
        if ( ! is_numeric( $author ) ) return -1;
        return ( (int) $author === (int) $this->user->ID );
    }

    public function cleanup_transient() {
        $name = 'gmce_thumb_votes_hour';
        $data = array_filter( (array) get_transient( $name ) );
        $clean = [ ];
        if ( ! empty( $data ) ) {
            foreach ( $data as $i => $item ) {
                if ( ( $item['ts'] + HOUR_IN_SECONDS ) > current_time( 'timestamp', TRUE ) ) {
                    $clean[$i] = $item;
                }
            }
        }
        set_transient( $name, $clean );
    }

    protected function thumb( $rev = NULL, $dir = 1 ) {
        $revid = $this->pre_thumb( $rev );
        if ( ! is_int( $revid ) ) return $revid;
        $votes = $this->get_votes( $rev );
        $key = $dir > 0 ? 'up' : 'down';
        $other = $dir > 0 ? 'down' : 'up';
        if ( ( $other_key = array_search( $this->user->ID, $votes[$other] ) ) !== FALSE ) {
            $action = 'undo';
            unset( $votes[$other][$other_key] );
        } else {
            $action = 'voting';
            $votes[$key][] = $this->user->ID;
        }
        $votes['score'] += $dir;
        if ( update_post_meta( $revid, static::$meta_key, $votes ) ) {
            $data = [
                'what' => "{$action}_{$key}",
                'dir'  => $dir,
                'from' => $this->user->ID,
                'to'   => get_post_field( 'post_author', $revid ),
                'for'  => $revid
            ];
            do_action( 'gmce_thumb_voted', $data, $action );
            do_action( "gmce_thumb_voted_{$action}", $data );
            $this->votes = $votes;
            return TRUE;
        }
        return FALSE;
    }

    protected function get_rev( $rev = NULL ) {
        $revid = FALSE;
        if ( $rev instanceof Revision && isset( $rev->ID ) ) {
            $revid = $rev->ID;
        } elseif ( $rev instanceof \WP_Post ) {
            if ( $rev->post_type !== \Brain\Container::instance()->get( 'cpt' ) ) return FALSE;
            $revid = $rev->ID;
        } elseif ( is_numeric( $rev ) ) {
            $revid = $rev;
        }
        return ! empty( $revid ) ? (int) $revid : FALSE;
    }

    protected function get_voters_info( $votes, $w = 'up' ) {
        $votes[$w] = array_filter( $votes[$w] );
        if ( empty( $votes[$w] ) ) return [ ];
        $info = [ ];
        $format = '<a href="%s" target="_blank">%s</a>';
        foreach ( $votes[$w] as $uid ) {
            $user = get_userdata( $uid );
            $url = get_author_posts_url( $uid, $user->user_nicename );
            $info[] = sprintf( $format, esc_url( $url ), esc_html( $user->display_name ) );
        }
        return $info;
    }

    protected function pre_thumb( $rev ) {
        if ( $this->user->is_banned() ) return 'banned';
        $is_author = $this->is_author( $rev );
        if ( $is_author === -1 ) return FALSE;
        if ( $is_author === TRUE ) return 'is_author';
        $revid = $this->get_rev( $rev );
        if ( ! is_int( $revid ) ) return FALSE;
        $post = get_post_meta( $revid, '__gmce_post', TRUE ) ? : 0;
        $post_author = (int) get_post_field( 'post_author', $post );
        $post_author_allowed = $this->settings->get( 'post_author_can_vote', TRUE );
        if ( ($post_author === $this->user->ID) && ! $post_author_allowed ) return 'is_post_author';
        if ( ! $this->user->is_admin ) {
            $bycookie = $this->checkCookie( $revid );
            if ( $bycookie === 0 ) return 'too_fast';
            if ( $bycookie === FALSE ) return 'too_times';
            if ( ! $this->checkTimes( $revid ) ) return 'went_banned';
        }
        return $revid;
    }

    protected function checkCookie( $revid ) {
        $return = TRUE;
        $name = 'gm_' . md5( "votes_{$revid}_{$this->user->ID}" );
        $now = current_time( 'timestamp', TRUE );
        $cookie = filter_input( INPUT_COOKIE, $name, FILTER_SANITIZE_STRING );
        if ( ! empty( $cookie ) ) {
            $data = explode( '|', $cookie );
            $last = is_numeric( $data[0] ) ? $data[0] : $now;
            $times = isset( $data[1] ) && is_numeric( $data[1] ) ? (int) $data[1] : 0;
        } else {
            $last = 0;
            $times = 0;
        }
        if ( ( $now - $last ) > 10 ) {
            $max = $this->settings->get( 'max_thumb_votes_hour', 3 );
            if ( ! is_numeric( $max ) || (int) $max < 1 ) $max = 1;
            if ( (int) $times >= (int) $max ) $return = FALSE;
        } elseif ( ( $now - $last ) < 10 ) {
            $return = 0;
        }
        if ( $return ) $times ++;
        setcookie( $name, "{$now}|{$times}", $now + HOUR_IN_SECONDS, '/' );
        return $return;
    }

    protected function checkTimes( $rev ) {
        $name = 'gmce_thumb_votes_hour';
        $data = array_filter( (array) get_transient( $name ) );
        $now = (int) current_time( 'timestamp', TRUE );
        $k = "{$rev}_{$this->user->ID}";
        if ( empty( $data ) || ! isset( $data[$k] ) ) $data[$k] = ['ts' => $now, 'n' => 0 ];
        if ( ! isset( $data[$k]['ts'] ) || ! is_numeric( $data[$k]['ts'] ) ) $data[$k]['ts'] = $now;
        if ( ! isset( $data[$k]['n'] ) || ! is_numeric( $data[$k]['n'] ) ) $data[$k]['n'] = 0;
        if ( ( $now - (int) $data[$k]['ts'] ) >= HOUR_IN_SECONDS ) {
            $data[$k]['ts'] = $now;
            $data[$k]['n'] = 0;
        }
        $max = $this->settings->get( 'max_thumb_votes_hour', 3 );
        if ( ! is_numeric( $max ) || (int) $max < 1 ) $max = 1;
        if ( (int) $data[$k]['n'] >= (int) $max ) {
            do_action( 'gmce_user_thumb_votes_banned', $this->user->ID, $rev );
            $this->user->autoban();
            unset( $data[$k] );
            $return = FALSE;
        } else {
            $data[$k]['n'] ++;
            $return = TRUE;
        }
        set_transient( $name, $data );
        return $return;
    }

}