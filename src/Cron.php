<?php

namespace GMCE;

/**
 * Cron Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Cron {

    protected static $prefix = 'gmce_cron_';

    protected $tasks = [ 'hourly' => [ ], 'twicedaily' => [ ], 'daily' => [ ] ];

    public function install() {
        do_action( 'gmce_cron_recurrences', $this );
        foreach ( $this->getRecurrences() as $recurrence ) {
            wp_schedule_event( time(), $recurrence, static::$prefix . $recurrence );
        }
    }

    public function unistall() {
        do_action( 'gmce_cron_recurrences', $this );
        foreach ( $this->getRecurrences() as $recurrence ) {
            wp_clear_scheduled_hook( static::$prefix . $recurrence );
        }
    }

    public function setup() {
        do_action( 'gmce_cron_recurrences', $this );
        do_action( 'gmce_cron', $this );
        foreach ( $this->getRecurrences() as $action ) {
            add_action( static::$prefix . $action, [ $this, 'perform' ] );
        }
    }

    public function addTask( $when = 'daily', $callable = NULL, $data = [ ] ) {
        if ( ! in_array( $when, $this->getRecurrences() ) || ! is_callable( $callable ) ) return;
        if ( ! is_array( $data ) ) $data = [ ];
        $this->tasks[ $when ][] = [ $callable, $data ];
    }

    public function perform() {
        $when = str_replace( static::$prefix, '', current_filter() );
        if ( $this->haveTasks( $when ) ) {
            foreach ( $this->tasks[ $when ] as $action ) {
                if ( ! is_array( $action ) || empty( $action ) || ! is_callable( $action[ 0 ] ) ) {
                    continue;
                }
                $data = isset( $action[ 1 ] ) && is_array( $action[ 1 ] ) ? $action[ 1 ] : [ ];
                call_user_func_array( $action[ 0 ], $data );
            }
            $this->removeTasks( $when );
        }
    }

    public function haveTasks( $when ) {
        if ( ! is_string( $when ) ) return FALSE;
        return in_array( $when, $this->getRecurrences() ) && ! empty( $this->tasks[ $when ] );
    }

    public function removeTasks( $when ) {
        if ( $this->haveTasks( $when ) ) {
            $this->tasks[ $when ] = [ ];
        }
    }

    public function addReccurrence( $recurrence = '' ) {
        if ( ! is_string( $recurrence ) || empty( $recurrence ) ) return;
        $this->tasks[ $recurrence ] = [ ];
    }

    public function addReccurrences( $recurrences = [ ] ) {
        if ( ! is_array( $recurrences ) || empty( $recurrences ) ) return;
        foreach ( $recurrences as $recurrence ) {
            $this->addReccurrence( $recurrence );
        }
    }

    function getRecurrences() {
        if ( ! did_action( 'gmce_cron_recurrences' ) ) do_action( 'gmce_cron_recurrences', $this );
        return array_keys( $this->tasks );
    }

}