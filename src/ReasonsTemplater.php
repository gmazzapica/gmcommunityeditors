<?php

namespace GMCE;

use Brain\Container as Brain;

/**
 * ReasonsTemplater Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class ReasonsTemplater {

    protected $user;

    protected $settings;

    protected $templates;

    function __construct( SettingsGMCE $s, User $u ) {
        $this->user = $u;
        $this->settings = $s;
        $this->templates = $this->settings->get( 'reason_templates', [ ] );
    }

    function setup() {
        if ( empty( $this->templates ) ) return;
        $should = TRUE;
        if ( ! $this->user->is_admin ) {
            $revs = Brain::instance()->get( 'api' )->get_user_votes( $this->user->ID, 'revisions' );
            $min = $this->settings->get( 'revisions_before_template', 2 );
            $should = (int) $revs >= (int) $min;
        }
        if ( $should ) {
            add_action( 'gmce_box_deny_after_textarea', [ $this, 'print_template' ] );
        }
    }

    function print_template() {
        $this->template_start();
        $format = '<option style="padding:0 10px;" value="%s">%s </option>';
        foreach ( $this->templates as $title => $comment ) {
            if ( is_array( $comment ) ) {
                $this->print_option_group( $title, $comment );
            } elseif ( is_string( $comment ) ) {
                $this->print_option( $comment, $title );
            }
        }
        $this->template_end();
    }

    protected function template_start() {
        echo '<p style="margin:10px 0;">';
        echo '<select style="background-color:#fff!important;height:1.5em;">';
    }

    protected function template_end() {
        echo '</select>';
        echo '<a href="#" style="margin-left:15px;font-weight:bold;" id="gmce_comment_templates">';
        echo __( 'Add', 'gmce' ) . '</a>';
        echo ' | <a href="#" style="font-weight:bold;" id="gmce_comment_clean">';
        echo __( 'Reset', 'gmce' ) . '</a></p>';
    }

    protected function print_option( $comment, $title ) {
        if ( ! is_string( $comment ) || ! is_string( $comment ) ) return;
        $format = '<option style="padding:0 10px;" value="%s">%s </option>';
        printf( $format, esc_attr( $comment ), esc_attr( $title ) );
    }

    protected function print_option_group( $group_title, $comment ) {
        if ( ! is_string( $group_title ) || ! is_array( $comment ) ) return;
        printf( '<optgroup label="%s">', esc_attr( $group_title ) );
        foreach ( $comment as $title_c => $comment_c ) {
            $this->print_option( $comment_c, $title_c );
        }
        echo '</optgroup>';
    }

}