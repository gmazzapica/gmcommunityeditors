<?php

namespace GMCE\Base;

/**
 * Settings Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Settings {

    protected $filter_prefix = '';

    protected $settings = [ ];

    function get_all() {
        return apply_filters( $this->filter_prefix . 'settings', $this->settings );
    }

    function set_all( $settings = [ ] ) {
        $this->settings = $settings;
    }

    function get( $w, $default = NULL ) {
        $v = NULL;
        if ( array_key_exists( $w, $this->settings ) && ! is_null( $this->settings[ $w ] ) ) {
            $v = $this->settings[ $w ];
        } elseif ( ! is_null( $default ) ) {
            $v = $default;
        }
        return apply_filters( $this->filter_prefix . $w . '_setting', $v );
    }

    function set( $w, $value ) {
        $this->settings[ $w ] = $value;
    }

    function merge( $settings = [ ] ) {
        $this->settings = wp_parse_args( $settings, $this->settings );
    }

    function load_file( $file, $setting = '' ) {
        $data = file_exists( $file ) ? require( $file ) : NULL;
        $i = pathinfo( $file );
        if ( is_null( $data ) ) {
            $i = pathinfo( $file );
            if ( $i[ 'extension' ] === 'php' ) {
                $defaults = "{$i[ 'dirname' ]}/{$i[ 'filename' ]}.defaults";
                $data = file_exists( $defaults ) ? require( $defaults ) : NULL;
            }
        }
        if ( is_array( $data ) ) {
            if ( empty( $setting ) ) {
                $this->merge( $data );
            } elseif ( is_string( $setting ) ) {
                $this->set( $setting, $data );
            }
        }
    }

}