<?php

namespace GMCE\Base;

use Brain\Container as Brain;

/**
 * Providerable Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
abstract class Providerable {

    protected $settings;

    protected $user;

    protected $revisions;

    protected $votes;

    protected $thumb_votes;

    protected $msg;

    function provider() {
        $brain = Brain::instance();
        $this->settings = $brain[ 'settings' ];
        $this->user = $brain[ 'user' ];
        $this->revisions = $brain[ 'revisions' ];
        $this->votes = $brain[ 'votes' ];
        $this->thumb_votes = $brain[ 'thumb_votes' ];
        $this->msg = $this->settings->get( 'messages', [ ] );
    }

    function get_settings() {
        return $this->settings;
    }

    function get_user() {
        return $this->user;
    }

    function get_revs() {
        return $this->revisions;
    }

    function get_votes() {
        return $this->votes;
    }

    function get_thumb_votes() {
        return $this->thumb_votes;
    }

    function get_strings() {
        return $this->msg;
    }

}