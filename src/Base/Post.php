<?php

namespace GMCE\Base;

class Post {

    public $ID;

    public $post_author = 0;

    public $post_date = '0000-00-00 00:00:00';

    public $post_date_gmt = '0000-00-00 00:00:00';

    public $post_content = '';

    public $post_title = '';

    public $post_excerpt = '';

    public $post_status = 'publish';

    public $comment_status = 'closed';

    public $ping_status = 'closed';

    public $post_password = '';

    public $post_name = '';

    public $to_ping = '';

    public $pinged = '';

    public $post_modified = '0000-00-00 00:00:00';

    public $post_modified_gmt = '0000-00-00 00:00:00';

    public $post_content_filtered = '';

    public $post_parent = 0;

    public $guid = '';

    public $menu_order = 0;

    public $post_type = 'post';

    public $post_mime_type = '';

    public $comment_count = 0;

    public $filter;

    public $format_content;

    public function setup( $post ) {
        foreach ( \get_object_vars( $post ) as $key => $value ) {
            if ( is_numeric( $value ) ) $value = (int) $value;
            $this->$key = $value;
        }
    }

}