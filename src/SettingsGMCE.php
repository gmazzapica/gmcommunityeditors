<?php

namespace GMCE;

use Brain\Container as Brain;

/**
 * SettingsGMCE Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class SettingsGMCE extends Base\Settings {

    function __construct() {
        $this->filter_prefix = 'gmce_';
    }

    function setup() {
        $path = Brain::instance()->get( 'path' );
        $files = include ( $path . 'settings/load.php');
        if ( ! empty( $files ) && is_array( $files ) ) {
            foreach ( $files as $file => $var ) {
                $this->load_file( $path . $file, $var );
            }
        }
    }

}