<?php namespace GMCE;

/**
 * Installer Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Installer {

    protected $settings;
    protected $cron;
    protected static $caps = [ 'gmce_promote', 'gmce_demote' ];
    protected static $allowed = [ 'administrator' ];

    public function __construct( SettingsGMCE $s, Cron $c ) {
        $this->settings = $s;
        $this->cron = $c;
    }

    function install() {
        $role = get_role( 'contributor' );
        $newcaps = array_combine( self::$caps, array_fill( 0, count( self::$caps ), TRUE ) );
        $role_name = $this->settings->get( 'role_name', 'community_editor' );
        $role_label = $this->settings->get( 'role_label', __( 'Community Editor', 'gmce' ) );
        add_role( $role_name, $role_label, array_merge( $role->capabilities, $newcaps ) );
        $roles_obj = new \WP_Roles();
        foreach ( self::$allowed as $role_name ) {
            foreach ( self::$caps as $cap ) {
                $roles_obj->add_cap( $role_name, $cap );
            }
        }
        $this->cron->install();
        do_action( 'gmce_installed' );
    }

    function unistall() {
        remove_role( $this->settings->get( 'role_name', 'community_editor' ) );
        $roles_obj = new \WP_Roles();
        $roles = self::$allowed;
        foreach ( $roles as $role_name ) {
            foreach ( self::$caps as $cap ) {
                $roles_obj->remove_cap( $role_name, $cap );
            }
        }
        $this->cron->unistall();
        do_action( 'gmce_unistalled' );
    }

}