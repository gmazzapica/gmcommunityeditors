<?php

namespace GMCE;

use Brain\Container as Brain;

/**
 * Revisions Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Revisions {

    protected $settings;

    protected $user;

    public $single;

    protected $reasons;

    protected $public;

    function __construct( SettingsGMCE $s, User $u, Revision $r, Reasons $re ) {
        $this->settings = $s;
        $this->user = $u;
        $this->single = $r;
        $this->reasons = $re;
    }

    function setup() {
        $this->reasons->setup();
        add_action( 'init', [ $this, 'register' ], 5 );
    }

    function register() {
        register_post_type( Brain::instance()->get( 'cpt' ), [
            'label'              => 'Community Revisions',
            'labels'             => [ 'menu_name' => 'Comm. Revisions' ],
            'public'             => $this->is_cpt_public(),
            'show_ui'            => $this->is_cpt_public(),
            'show_in_menu'       => $this->is_cpt_public(),
            'publicly_queryable' => TRUE,
            'query_var'          => TRUE,
            'capability_type'    => 'post',
            'has_archive'        => TRUE,
            'hierarchical'       => TRUE,
            'supports'           => [ 'title' ]
        ] );
    }

    function get( $posts = NULL, $user = NULL, $status = 'publish', $exclude_final = TRUE ) {
        $parsed = [ ];
        $cpt = Brain::instance()->get( 'cpt' );
        $tax = Brain::instance()->get( 'tax' );

        $def = [ 'post_type' => $cpt, 'post_status' => 'publish', 'posts_per_page' => -1 ];
        if ( $exclude_final ) {
            $parsed['tax_query'] = [
                [
                    'taxonomy' => $tax,
                    'field'    => 'slug',
                    'terms'    => [ '_final_revision_' ],
                    'operator' => 'NOT IN'
                ]
            ];
        }
        if ( is_numeric( $user ) ) {
            $user = (int) $user;
            $parsed['author'] = $user;
        }
        if ( ! empty( $posts ) ) {
            $mq = $this->setup_posts_qa( $posts );
            if ( ! empty( $mq ) ) $parsed['meta_query'] = [ $mq ];
        }
        if ( $status === 'any' || $status === 'trash' ) $parsed['post_status'] = $status;
        $q = new \WP_Query( wp_parse_args( $parsed, $def ) );
        $converted = (int) $q->found_posts ? array_map( [ $this, 'convert' ], $q->posts ) : 0;
        return is_array( $converted ) ? array_filter( $converted ) : [ ];
    }

    function user_revs( $user = null, $status = 'publish', $no_final = TRUE ) {
        if ( is_numeric( $user ) ) $uid = (int) $user;
        if ( is_object( $user ) && isset( $user->ID ) ) $uid = (int) $user->ID;
        if ( isset( $uid ) ) return $this->get( null, $uid, $status, $no_final );
    }

    function post_revs( $post = null, $status = 'publish', $no_final = TRUE ) {
        if ( is_numeric( $post ) && intval( $post ) ) $pid = (int) $post;
        if ( is_object( $post ) && isset( $post->ID ) ) $pid = (int) $post->ID;
        if ( isset( $pid ) ) return $this->get( $pid, null, $status, $no_final );
    }

    function user_post_rev( $user = null, $post = null, $status = 'publish', $no_final = TRUE ) {
        if ( is_numeric( $user ) ) $uid = (int) $user;
        if ( is_object( $user ) && isset( $user->ID ) ) $uid = (int) $user->ID;
        if ( is_int( $post ) ) $pid = (int) $post;
        if ( is_object( $post ) && isset( $post->ID ) ) $pid = (int) $post->ID;
        if ( isset( $pid ) && isset( $uid ) ) {
            $posts = $this->get( $pid, $uid, $status, $no_final );
            $post = ! empty( $posts ) ? array_pop( $posts ) : FALSE;
            return ( $post ) ? $post : null;
        }
    }

    function convert( \WP_Post $post ) {
        $class = get_class( $this->single );
        return $class::convert( $post );
    }

    protected function setup_posts_qa( $posts ) {
        if ( ! is_array( $posts ) ) $posts = [ $posts ];
        $ids = array_unique( array_filter( $posts, 'is_numeric' ) );
        $i = array_search( 0, $ids );
        if ( $i !== FALSE ) unset( $ids[$i] );
        $ids_str = array_map( function( $id ) {
            return sprintf( '%s', $id );
        }, $ids );
        if ( ! empty( $ids_str ) ) {
            return [
                'key'     => '__gmce_post',
                'value'   => $ids_str,
                'type'    => 'NUMERIC',
                'compare' => 'IN'
            ];
        }
    }

    function add_revision_comment( $revision, $postid, $comment ) {
        $check = $this->check_comment( $revision, $postid, $comment );
        if ( $check !== TRUE ) return $check;
        $msgs = $this->settings->get( 'messages', [ ] );
        $comment = wp_kses( nl2br( $comment ), $this->settings->get( 'allowed_html', [ ] ) );
        $save = $revision->set_comment( $comment );
        $msg = $save ? 'add_comment_success' : 'add_comment_error';
        return [
            'post'     => $postid,
            'revision' => $revision->ID,
            'status'   => $save ? 'success' : 'error',
            'message'  => $msgs['ajax_vote_reponses'][$msg]
        ];
    }

    function check_comment( $revision, $postid, $comment ) {
        $msgs = $this->settings->get( 'messages', [ ] );
        if ( empty( $comment ) ) return $msgs['ajax_vote_reponses']['error'];
        if ( ! $postid > 0 ) return $msgs['ajax_vote_reponses']['error'];
        if ( ! $this->user->can_box ) return $msgs['ajax_vote_reponses']['notcan'];
        if ( ! $revision instanceof \GMCE\Revision ) return $msgs['ajax_vote_reponses']['error'];
        return TRUE;
    }

    function is_cpt_public() {
        $allow = $this->settings->get( 'rev_post_type_public', FALSE );
        return $allow && $this->user->is_admin;
    }

}