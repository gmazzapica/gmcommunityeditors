<?php namespace GMCE;

use Brain\Container as Brain;

/**
 * Revision Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Revision extends Base\Post {

    protected $settings;

    protected $user;

    protected $thumb_votes;

    public $reasons;

    public $title;

    public $content;

    public $reason;

    public $author;

    public $time;

    static function convert( \WP_Post $post ) {
        $class = __CLASS__;
        $s = Brain::instance()->get( 'settings' );
        $u = Brain::instance()->get( 'user' );
        $r = Brain::instance()->get( 'reasons' );
        $v = Brain::instance()->get( 'thumb_votes' );
        $obj = new $class( $s, $u, $r, $v );
        return $obj->get( $post );
    }

    function __construct( SettingsGMCE $s, User $u, Reasons $r, ThumbVotes $v ) {
        $this->settings = $s;
        $this->user = $u;
        $this->reasons = $r;
        $this->thumb_votes = $v;
    }

    function __get( $name ) {
        if ( $name === 'comments' ) return $this->get_comments();
        if ( $name === 'info' ) return array_filter( get_object_vars( $this ), 'is_scalar' );
        if ( $name === 'post' ) return (int) get_post_meta( $this->ID, '__gmce_post', TRUE );
        if ( $name === 'fixed' ) return (bool) get_post_meta( $this->ID, '__gmce_fixed', TRUE );
        if ( $name === 'onhold' ) return (bool) get_post_meta( $this->ID, '__gmce_onhold', TRUE );
    }

    function __toString() {
        return $this->title;
    }

    function get( $post ) {
        if ( is_numeric( $post ) && intval( $post ) ) $post = get_post( (int) $post );
        if ( $post instanceof \WP_Post ) {
            $this->setup( $post );
            $this->filter_content();
        }
        return $this;
    }

    function create( $post, $reason, $content ) {
        if ( ! $this->can_create( $post ) || ! $this->reasons->is_good( $reason ) ) return FALSE;
        $args = $this->prepare_args( $post, $reason, $content );
        $id = wp_insert_post( $args );
        if ( (int) $id > 0 ) $this->created( $id, $post, $reason );
    }

    function update( $args ) {
        if ( $this->ID ) $args['ID'] = $this->ID;
        if ( isset( $args['reason'] ) && is_string( $args['reason'] ) ) {
            $reason = $args['reason'];
            unset( $args['reason'] );
        }
        if ( wp_update_post( $args ) ) $this->set_reason( $reason );
    }

    function delete( $force = FALSE ) {
        if ( $force ) return $this->force_delete();
        global $wpdb;
        return $wpdb->query( $wpdb->prepare(
                    "UPDATE $wpdb->posts SET post_status = 'trash' WHERE ID = %d", $this->ID
            ) );
    }

    function get_author_name() {
        if ( ! $this->ID ) return;
        $user = new \WP_User( $this->post_author );
        return $user->display_name;
    }

    function get_reason() {
        if ( ! $this->ID ) return;
        $tax = Brain::instance()->get( 'tax' );
        $r = wp_get_object_terms( $this->ID, $tax, [ 'fields' => 'all' ] );
        if ( is_array( $r ) && ! empty( $r ) ) return array_pop( $r );
    }

    function set_reason( $reason ) {
        $tax = Brain::instance()->get( 'tax' );
        $termObj = get_term_by( 'slug', $reason, $tax );
        if ( empty( $termObj ) && taxonomy_exists( $tax ) ) {
            $insert = (array) $this->reasons->insert( $reason );
            $term_id = isset( $insert['term_id'] ) ? $insert['term_id'] : FALSE;
        } elseif ( is_object( $termObj ) && ! is_wp_error( $termObj ) ) {
            $term_id = $termObj->term_id;
        }
        if ( $term_id ) {
            wp_set_object_terms( $this->ID, (int) $term_id, $tax, FALSE );
        }
    }

    function get_comments() {
        global $wpdb;
        $order = $this->settings->get( 'comment_default_order', 'DESC' );
        if ( ! in_array( strtoupper( $order ), [ 'ASC', 'DESC' ] ) ) $order = 'DESC';
        $comments = $wpdb->get_results( $wpdb->prepare(
                "SELECT * FROM $wpdb->comments WHERE comment_post_ID = %d "
                . "AND comment_approved = 'gmce' "
                . "AND (comment_type = 'gmce' OR comment_type = 'gmce_deleted') "
                . "ORDER BY comment_date " . strtoupper( $order ), $this->ID
            ) );
        return array_map( [ $this, 'filter_comment' ], $comments );
    }

    function can_fix() {
        $reason = $this->get_reason();
        if ( empty( $reason ) || ! $this->reasons->is_good( $reason->slug ) ) return FALSE;
        if ( $this->user->is_admin ) return TRUE;
        $file = (array) $this->reasons->file_info( $reason->slug );
        $can = isset( $file['can_fix'] ) ? $file['can_fix'] : 'default';
        $is_author = ( $this->post_author === $this->user->ID );
        $is_p_author = (int) $this->user->ID === (int) get_post_field( 'post_author', $this->post );
        switch ( $can ) {
            case 'admins' :
                return FALSE;
            case 'all' :
                return $is_p_author || $this->user->is_ce;
            case 'editors' :
                return ! $is_p_author && $this->user->is_ce;
            case 'others' :
                return ! $is_p_author && ! $is_author && $this->user->is_ce;
            case 'owner' :
                return $is_author;
            case 'post_author' :
                return $is_p_author;
            case 'default' :
            default:
                return $is_p_author || $is_author;
        }
        return FALSE;
    }

    function delete_comment( $comment_id ) {
        global $wpdb;
        $msg = $this->settings->get( 'messages', [ ] );
        return $wpdb->query( $wpdb->prepare(
                    "UPDATE $wpdb->comments SET comment_content = %s, comment_type='gmce_deleted' "
                    . "WHERE comment_ID = %d", $msg['frontend_localize']['deleted'], $comment_id
            ) );
    }

    function set_comment( $comment ) {
        if ( $this->ID ) {
            $time = current_time( 'mysql' );
            $data = [
                'comment_post_ID'   => (int) $this->ID,
                'comment_author'    => esc_sql( $this->user->display_name ),
                'comment_author_IP' => '',
                'comment_date'      => esc_sql( $time ),
                'comment_date_gmt'  => esc_sql( get_gmt_from_date( $time ) ),
                'comment_parent'    => 0,
                'comment_approved'  => 1,
                'comment_karma'     => 0,
                'user_id'           => (int) $this->user->ID,
                'comment_type'      => 'gmce',
                'comment_approved'  => 'gmce',
                'comment_content'   => $comment,
            ];
            global $wpdb;
            return $wpdb->insert( $wpdb->comments, $data );
        }
    }

    function has_comments() {
        global $wpdb;
        return $wpdb->get_var( $wpdb->prepare(
                    "SELECT COUNT(comment_ID) FROM $wpdb->comments WHERE comment_post_ID = %d "
                    . "AND comment_approved = 'gmce' "
                    . "AND (comment_type = 'gmce' OR comment_type = 'gmce_deleted')", $this->ID
            ) );
    }

    function can_create( $post ) {
        if ( ! $post instanceof \WP_Post ) return FALSE;
        if ( (int) $post->post_author === (int) $this->user->ID ) return FALSE;
        if ( ! in_array( $post->post_type, $this->settings->get( 'post_types', [ 'post' ] ) ) ) {
            return FALSE;
        }
        if ( ! $this->user->is_admin && ! $this->user->is_ce ) return FALSE;
        return TRUE;
    }

    function set_fixed() {
        $votes = get_post_meta( $this->post, '__gmce_votes', TRUE );
        $votes['score'] += 2;
        $votes['fixed'] += 1;
        wp_cache_delete( "votes_info_{$this->post}", 'gmce' );
        update_post_meta( $this->post, '__gmce_votes', $votes );
        update_post_meta( $this->ID, '__gmce_fixed', '1' );
        if ( (int) $votes['score'] === $this->settings->get( 'vote_limit', 10 ) ) {
            \Brain\Container::instance()->get( 'auto_publisher' )->enqueue( $this->post );
        }
        return true;
    }

    function set_unfixed() {
        $votes = get_post_meta( $this->post, '__gmce_votes', TRUE );
        $votes['score'] -= 2;
        $votes['fixed'] -= 1;
        wp_cache_delete( "votes_info_{$this->post}", 'gmce' );
        update_post_meta( $this->post, '__gmce_votes', $votes );
        \Brain\Container::instance()->get( 'auto_publisher' )->dequeue( $this->post );
        return delete_post_meta( $this->ID, '__gmce_fixed' );
    }

    function get_fixed() {
        return get_post_meta( $this->ID, '__gmce_fixed', TRUE );
    }

    function get_post_info( $post ) {
        $i = get_object_vars( $post );
        foreach ( $i as $k => $v ) {
            if ( substr_count( $k, 'post_' ) ) {
                $nk = str_replace( 'post_', '', $k );
                $i[$nk] = wp_filter_nohtml_kses( $v );
            }
        }
        return $i;
    }

    function get_thumb_score() {
        return $this->thumb_votes->get_score( $this->ID );
    }

    function get_thumb_votes() {
        return $this->thumb_votes->get_votes( $this->ID );
    }

    function get_thumb_uppers() {
        return $this->thumb_votes->get_voters( $this->ID, 'up' );
    }

    function get_thumb_downers() {
        return $this->thumb_votes->get_voters( $this->ID, 'down' );
    }

    function currentuser_voted_up() {
        return $this->thumb_votes->user_voted_up( $this->ID );
    }

    function currentuser_voted_down() {
        return $this->thumb_votes->user_voted_down( $this->ID );
    }

    function as_post() {
        return new \WP_Post( $this );
    }

    protected function force_delete() {
        $reason = $this->get_reason();
        global $wpdb;
        $a = $wpdb->query( $wpdb->prepare( "DELETE FROM $wpdb->posts WHERE ID = %d", $this->ID ) );
        $b = $wpdb->query( $wpdb->prepare(
                "DELETE FROM $wpdb->comments WHERE comment_post_ID = %d "
                . "AND comment_approved = 'gmce'", $this->ID
            ) );
        $c = $wpdb->query( $wpdb->prepare(
                "DELETE FROM $wpdb->postmeta WHERE post_id = %d", $this->ID
            ) );
        $count = $reason->count ? (int) $reason->count - 1 : 0;
        $tid = $reason->term_id;
        $d = $wpdb->query( $wpdb->prepare(
                "UPDATE $wpdb->term_taxonomy SET count = %d WHERE term_id = %d", $count, $tid
            ) );
        return ( $a && $b && $c && $d);
    }

    protected function created( $id, $post, $reason ) {
        $created = get_post( $id );
        $this->setup( $created );
        $i = $this->get_post_info( $post );
        update_post_meta( $id, '__gmce_post', $i['ID'] );
        $this->set_reason( $reason );
    }

    protected function filter_content() {
        if ( ! $this->ID ) return;
        $this->title = $this->post_title;
        $this->content = wpautop( wptexturize( $this->post_content ) );
        if ( empty( $this->post_excerpt ) ) {
            $num = $this->settings->get( 'revisions_excerpt_words', 25 );
            $raw = wptexturize( wp_filter_nohtml_kses( $this->content ) );
            $summary = wp_trim_words( $raw, $num, '...' );
            $this->summary = $summary;
        } else {
            $this->summary = $this->post_excerpt;
        }
        $this->author = $this->get_author_name();
        $this->author_url = get_author_posts_url( $this->post_author );
        $reason = $this->get_reason();
        $this->reason = $reason->name;
        $this->reason_slug = $reason->slug;
        $this->time = gmce_human_post_time( $this->ID );
    }

    function filter_comment( $comment ) {
        $comment->content = wpautop( wptexturize( $comment->comment_content ) );
        $comment->author = $comment->comment_author;
        $comment->author_url = get_author_posts_url( $comment->user_id );
        $comment->time = gmce_human_post_time( FALSE, $comment );
        $comment->timestamp = strtotime( $comment->comment_date );
        return $comment;
    }

    protected function prepare_args( $post, $reason, $content ) {
        $msg = $this->settings->get( 'messages', [ ] );
        $tf = $msg['revision_title_format'];
        $i = $this->get_post_info( $post );
        $oauthor = new \WP_User( $i['author'] );
        $author = $this->user->user->display_name;
        $r_i = $this->reasons->info( $reason );
        $title = sprintf( $tf, $i['title'], $oauthor->display_name, $author, $r_i['name'] );
        $num = $this->settings->get( 'revisions_excerpt_words', 25 );
        $exc = wptexturize( wp_filter_nohtml_kses( $content ) );
        return [
            'post_type'    => Brain::instance()->get( 'cpt' ),
            'post_title'   => $title,
            'post_name'    => sanitize_title( $title ),
            'post_content' => $content,
            'post_excerpt' => wp_trim_words( $exc, $num, '...' ),
            'post_author'  => $this->user->ID,
            'post_date'    => current_time( 'mysql' ),
            'post_status'  => 'publish'
        ];
    }

}