<?php

namespace GMCE;

use Brain\Container as Brain;

/**
 * Reasons Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Reasons {

    protected $settings;

    protected $tax;

    protected $cpt;

    protected $public;

    protected $reasons;

    function __construct( SettingsGMCE $s ) {
        $this->settings = $s;
    }

    function setup() {
        $this->tax = Brain::instance()->get( 'tax' );
        $this->cpt = Brain::instance()->get( 'cpt' );
        $allow = $this->settings->get( 'reason_tax_public', FALSE );
        $this->public = $allow && Brain::instance()->get( 'user' )->is_admin;
        $this->reasons = $this->settings->get( 'reasons', [ ] );
        $this->reasons['_final_revision_'] = [ 'Final Revision', '' ];
        add_action( 'init', [ $this, 'register' ] );
        add_action( 'init', [ $this, 'sync_reasons' ], 15 );
    }

    function register() {
        register_taxonomy( $this->tax, $this->cpt, [
            'label'             => 'Reasons',
            'public'            => $this->public,
            'show_ui'           => $this->public,
            'show_admin_column' => $this->public,
            'show_in_nav_menus' => FALSE,
            'query_var'         => TRUE,
            'hierarchical'      => TRUE,
            'capabilities'      => [
                'manage_terms' => $this->settings->get( 'admin_cap', 'edit_others_posts' ),
                'edit_terms'   => 'gmce_demote',
                'delete_terms' => 'gmce_demote',
                'assign_term'  => 'gmce_demote'
            ]
        ] );
    }

    function sync_reasons() {
        if ( ! empty( $this->reasons ) ) {
            $saved = get_terms( $this->tax, [ 'hide_empty' => FALSE ] );
            $slugs = ! empty( $saved ) && is_array( $saved ) ? wp_list_pluck( $saved, 'slug' ) : [ ];
            $notsaved = array_diff( array_keys( $this->reasons ), $slugs );
            $deprecated = array_diff( $slugs, array_keys( $this->reasons ) );
            $this->insert_reasons( $notsaved );
            $this->remove_reasons( $deprecated );
        }
    }

    function insert( $reason ) {
        $info = $this->info( $reason );
        if ( ! empty( $info ) ) {
            $name = $info['name'];
            unset( $info['name'] );
            return wp_insert_term( $name, $this->tax, $info );
        }
    }

    function delete( $reason ) {
        $term = get_term_by( 'slug', $reason, $this->tax );
        if ( $term->count == 0 ) wp_delete_term( $term->term_id, $this->tax );
    }

    function file_info( $reason ) {
        if ( $this->is_good( $reason ) ) {
            return (array) $this->reasons[$reason];
        }
    }

    function info( $reason ) {
        $info = [ 'name' => '', 'slug' => '', 'description' => '' ];
        $file = $this->file_info( $reason );
        if ( ! empty( $file ) && is_array( $file ) ) {
            $name = empty( $file[0] ) ? ucwords( $reason ) : $file[0];
            $desc = isset( $file[1] ) && is_string( $file[1] ) ? esc_html( $file[1] ) : '';
            $info = [ 'name' => $name, 'slug' => $reason, 'description' => $desc ];
        }
        return $info;
    }

    function is_good( $reason ) {
        return is_string( $reason ) && array_key_exists( $reason, $this->reasons );
    }

    protected function insert_reasons( $notsaved ) {
        if ( empty( $notsaved ) ) return;
        foreach ( $notsaved as $reason ) {
            $this->insert( $reason );
        }
    }

    protected function remove_reasons( $reasons ) {
        if ( empty( $reasons ) ) return;
        foreach ( $reasons as $reason ) {
            $this->delete( $reason );
        }
    }

}