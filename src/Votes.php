<?php namespace GMCE;

/**
 * Votes Class
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica
 */
class Votes {

    protected $settings;

    protected $user;

    protected $revisions;

    protected $reasons;

    function __construct( SettingsGMCE $s, User $u, Revisions $r, Reasons $rs ) {
        $this->settings = $s;
        $this->user = $u;
        $this->revisions = $r;
        $this->reasons = $rs;
    }

    function vote_up( $post = NULL ) {
        return $this->vote( 'up', $post );
    }

    function vote_down( $post = NULL, $reason = NULL, $content = NULL ) {
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            $info = $this->get_vote_down_info_ajax();
        } else {
            $reason = wp_filter_nohtml_kses( $reason );
            $content = wp_kses( nl2br( $content ), $this->settings->get( 'allowed_html', [ ] ) );
            $info = [ 'reason' => $reason, 'content' => $content ];
        }
        if ( ! $this->reasons->is_good( $info['reason'] ) ) return $this->voted( 'error' );
        return $this->vote( 'down', $post, $info['reason'], $info['content'] );
    }

    function vote( $up_down, $post = NULL, $reason = NULL, $content = NULL ) {
        $post = $this->get_post( $post );
        $now = $this->get_post_votes( $post );
        $error = $this->check_vote( $up_down, $now, $post, $reason );
        if ( ! empty( $error ) ) return $this->voted( $error );
        $down_not_final = ( ($up_down === 'down') && ($reason !== '_final_revision_') );
        if ( $down_not_final && $this->user->had_voted( $now, 'up' ) ) {
            $this->undo_approval( $now, 'down', $post );
            $now = $this->get_post_votes( $post );
        }
        wp_cache_delete( "votes_info_{$post->ID}", 'gmce' );
        return $this->cast_vote( $now, $post, $up_down, $reason, $content );
    }

    function remove_vote( $post, $userid, $up_down, $revision = NULL ) {
        $post = $this->get_post( $post );
        wp_cache_delete( "votes_info_{$post->ID}", 'gmce' );
        $now = $this->get_post_votes( $post );
        $isfixed = FALSE;
        if ( $up_down === 'down' && ( $revision instanceof Revision ) ) {
            $isfixed = $revision->fixed;
            $revision->delete();
        }
        $votes = $this->unset_vote_metadata( $now, $post, $userid, $up_down, $isfixed );
        $this->lock( $post, $votes );
        $this->user->set_votes_info( $up_down, -1 );
        return $this->vote_data( $up_down, $votes, $post, FALSE, TRUE );
    }

    function votes_info( $post = NULL ) {
        if ( is_numeric( $post ) ) $postid = (int) $post;
        if ( $post instanceof \WP_Post ) $postid = (int) $post->ID;
        if ( ! is_int( $postid ) ) return;
        $cached = wp_cache_get( "votes_info_{$postid}", 'gmce' );
        if ( ! empty( $cached ) ) return $cached;
        $votes = $this->get_post_votes( $postid );
        $counts = $this->votes_count( $votes );
        $users = $this->approvers_and_deniers( $votes );
        $reasons = $this->votes_reasons( $votes );
        $others = [ 'score' => $votes['score'], 'locked' => $votes['locked'] ];
        $info = array_merge( $counts, $users, $reasons, $others );
        wp_cache_set( "votes_info_{$postid}", $info, 'gmce' );
        return $info;
    }

    function get_post_votes( $post ) {
        if ( is_numeric( $post ) ) $postid = (int) $post;
        if ( $post instanceof \WP_Post ) $postid = (int) $post->ID;
        if ( ! isset( $postid ) || ! is_int( $postid ) ) return;
        $defaults = [
            'up'               => [ ],
            'down'             => [ ],
            'score'            => 0,
            'down_reasons'     => [ ],
            'final_down_users' => [ ],
            'triple'           => [ ],
            'locked'           => '',
            'fixed'            => 0
        ];
        $saved = get_post_meta( $postid, '__gmce_votes', TRUE ) ? : FALSE;
        return $saved ? wp_parse_args( $saved, $defaults ) : $defaults;
    }

    function lock( $post, $votes = NULL, $force = FALSE ) {
        $post = $this->get_post( $post, TRUE );
        if ( empty( $post ) ) return;
        if ( empty( $votes ) ) $votes = $this->get_post_votes( $post );
        if ( ! $force ) $force = $this->should_lock( $post, $votes );
        if ( $force ) {
            wp_cache_delete( "votes_info_{$post}", 'gmce' );
            $votes['locked'] = 1;
            return update_post_meta( $post, '__gmce_votes', $votes );
        }
    }

    function unlock( $post, $votes = NULL, $force = FALSE ) {
        $post = $this->get_post( $post, TRUE );
        if ( empty( $post ) ) return;
        if ( empty( $votes ) ) $votes = $this->get_post_votes( $post );
        if ( ! $force ) $force = ! $this->should_lock( $post, $votes );
        if ( $force ) {
            wp_cache_delete( "votes_info_{$post}", 'gmce' );
            $votes['locked'] = '';
            return update_post_meta( $post, '__gmce_votes', $votes );
        }
    }

    function lock_or_not( $post, $votes = NULL, $force = FALSE ) {
        $this->lock( $post, $votes, $force );
        $this->unlock( $post, $votes, $force );
    }

    function should_lock( $post, $votes = NULL ) {
        if ( empty( $votes ) ) $votes = $this->get_post_votes( $post );
        $up = count( $votes['up'] );
        $down = count( $votes['down'] );
        return ( ( $up + $down ) >= $this->settings->get( 'vote_limit', 10 ) );
    }

    function clean_up( $post = NULL, $force = FALSE ) {
        $postid = $this->get_post( $post, TRUE );
        $votes = $this->get_post_votes( $postid );
        $this->clean_up_meta( $postid );
        $this->clean_up_revisions( $postid, $force );
        $this->clean_up_users( $votes );
        $this->dequeue( $post );
    }

    function clean_up_votes( $post = NULL, $w = 'down' ) {
        if ( $w !== 'down' && $w !== 'up' ) return;
        $postid = is_numeric( $post ) ? (int) $post : $this->get_post( $post, TRUE );
        wp_cache_delete( "votes_info_{$postid}", 'gmce' );
        $votes = $this->get_post_votes( $postid );
        $mod = -1;
        if ( $w === 'down' ) {
            $this->clean_up_revisions( $postid, TRUE );
            $votes['down_reasons'] = [ ];
            $mod = 1;
        }
        $this->clean_up_users( $votes, $w );
        $vv = $votes[$w];
        $votes[$w] = [ ];
        $votes['score'] = $votes['score'] + ( count( $vv ) * $mod );
        $votes['triple'] = array_diff( $votes['triple'], $vv );
        $votes['locked'] = $this->should_lock( $postid, $votes ) ? 1 : '';
        update_post_meta( $postid, '__gmce_votes', $votes );
        if ( $w === 'up' ) {
            $this->dequeue( $post );
        } else {
            $this->maybe_enqueue( $post, $votes['score'] );
        }
    }

    function clean_up_meta( $postid ) {
        wp_cache_delete( "votes_info_{$postid}", 'gmce' );
        $metas = [ 'post', 'votes' ];
        foreach ( $metas as $meta ) {
            delete_post_meta( $postid, '__gmce_' . $meta );
        }
    }

    function clean_up_revisions( $postid, $force = FALSE ) {
        wp_cache_delete( "votes_info_{$postid}", 'gmce' );
        $revs = $this->revisions->post_revs( $postid, 'any', $force );
        if ( ! empty( $revs ) ) {
            foreach ( $revs as $rev ) {
                $rev->delete( $force );
            }
        }
    }

    function clean_up_users( $votes, $which = NULL ) {
        $user = new User( $this->settings );
        $vv = ( ! empty( $which ) ) ? [ $which ] : [ 'up', 'down', 'final_down_users' ];
        foreach ( $vv as $v ) {
            foreach ( $votes[$v] as $userid ) {
                if ( ! (int) $userid > 0 ) continue;
                $user->ID = (int) $userid;
                $user->set_votes_info( $v, -1 );
            }
        }
    }

    function confirm_users_vote( $users = [ ], $up_down = 'down', $post = NULL, $dir = 1 ) {
        if ( empty( $users ) || ! is_array( $users ) ) return;
        $users = array_unique( $users );
        foreach ( $users as $user ) {
            $this->confirm_user_vote( $user, $up_down, $post, $dir );
        }
    }

    function confirm_user_vote( $userid = 0, $up_down = 'down', $post = NULL, $dir = 1 ) {
        if ( $up_down !== 'up' && $up_down !== 'down' ) return;
        $k = [ 'up', 'down', 'total', 'up_s', 'down_s', 'total_s', 'final' ];
        $def = array_combine( $k, array_fill( 0, count( $k ), 0 ) );
        $meta = wp_parse_args( get_user_meta( $userid, '__gmce_votes', TRUE ), $def );
        $w = $up_down === 'up' ? 'up_s' : 'down_s';
        $meta[$w] += $dir;
        $meta['total_s'] += $dir;
        update_user_meta( $userid, '__gmce_votes', $meta );
        do_action( 'gmce_confirm_user_vote', $userid, $up_down, $meta, $post, $dir );
    }

    protected function cast_vote( $now, $post, $up_down, $reason, $content = NULL ) {
        $votes = $this->set_vote_metadata( $now, $post, $up_down, $reason );
        $final = ( $reason === '_final_revision_' );
        if ( ! $final ) $this->lock( $post, $votes );
        $this->user->set_votes_info( $up_down, 1, $final );
        if ( $up_down === 'down' ) {
            $this->revisions->single->create( $post, $reason, $content );
        }
        return $this->vote_data( $up_down, $votes, $post, FALSE );
    }

    protected function undo_approval( $votes, $up_down, $post ) {
        if ( ! $this->settings->get( 'allow_undo_vote', TRUE ) ) {
            return $this->voted( 'noundo' );
        }
        if ( $up_down !== 'down' ) return $this->voted( 'error' );
        $postid = $this->get_post( $post, TRUE );
        wp_cache_delete( "votes_info_{$postid}", 'gmce' );
        $now = $this->get_undo_votes( $votes );
        $votes = $this->set_vote_metadata( $now, $post, NULL );
        $this->user->set_votes_info( 'up', -1 );
        return $this->vote_data( $up_down, $votes, $post, TRUE );
    }

    protected function set_vote_metadata( $votes, $post, $up_down, $reason = NULL ) {
        if ( $reason !== '_final_revision_' ) {
            $votes[$up_down][] = (int) $this->user->ID;
            $modscore = $up_down === 'up' ? 1 : -1;
            if ( $up_down === 'down' ) {
                $votes['down_reasons'][$this->user->ID] = $reason;
            } else {
                $maybe_enqueue = $this->settings->get( 'autopublish_enable', FALSE );
            }
        } else {
            $votes['final_down_users'][] = (int) $this->user->ID;
            $modscore = -2;
            $dequeue = $this->settings->get( 'autopublish_enable', FALSE );
        }
        $votes['score'] = $votes['score'] + $modscore;
        update_post_meta( $post->ID, '__gmce_votes', $votes );
        if ( isset( $maybe_enqueue ) && $maybe_enqueue ) {
            $this->maybe_enqueue( $post, $votes['score'] );
        }
        if ( isset( $dequeue ) && $dequeue ) {
            $this->dequeue( $post );
        }
        return $votes;
    }

    protected function unset_vote_metadata( $votes, $post, $userid, $up_down, $isfixed = FALSE ) {
        $i = array_search( $userid, $votes[$up_down] );
        if ( $i !== FALSE ) {
            unset( $votes[$up_down][$i] );
        }
        if ( $up_down === 'down' ) {
            $votes['fixed'] = $isfixed ? \absint( $votes['fixed'] - 1 ) : $votes['fixed'];
            $modscore = $isfixed ? -1 : 1;
        } else {
            $modscore = -1;
        }
        $votes['score'] = $votes['score'] + $modscore;
        unset( $votes['down_reasons'][$userid] );
        update_post_meta( $post->ID, '__gmce_votes', $votes );
        if ( $modscore > 0 ) {
            $this->maybe_enqueue( $post, $votes['score'] );
        } else {
            $this->dequeue( $post );
        }
        return $votes;
    }

    protected function maybe_enqueue( $post, $score = 0 ) {
        $limit = $this->settings->get( 'vote_limit', 10 );
        if ( (int) $score === (int) $limit ) {
            $postid = $this->get_post( $post, TRUE );
            $auto = \Brain\Container::instance()->get( 'auto_publisher' );
            $auto->enqueue( $postid );
        }
    }

    protected function dequeue( $post ) {
        $postid = $this->get_post( $post, TRUE );
        $auto = \Brain\Container::instance()->get( 'auto_publisher' );
        $auto->dequeue( $postid );
    }

    protected function vote_data( $up_down, $votes, $post, $undo = FALSE, $delete = FALSE ) {
        $up = count( $votes['up'] );
        $down = count( $votes['down'] );
        $final = count( $votes['final_down_users'] );
        $vote_data = [
            'up'            => $up,
            'down'          => $down,
            'score'         => $votes['score'],
            'total'         => $up + $down,
            'locked'        => $this->should_lock( $post, $votes ),
            'reasons'       => (object) array_count_values( $votes['down_reasons'] ),
            'final_reasons' => (object) [ '_final_revision_' => $final ],
        ];
        $k = $undo ? 'undo_' . $up_down : 'success_' . $up_down;
        if ( $delete ) $k .= '_delete';
        return $this->voted( $k, 'success', $vote_data );
    }

    protected function voted( $response = 'error', $status = 'error', $data = NULL ) {
        $messages = $this->settings->get( 'messages', [ ] );
        $msgs = $messages['ajax_vote_reponses'];
        $return = [
            'status'  => $status,
            'message' => isset( $msgs[$response] ) ? $msgs[$response] : $response,
            'msgType' => $response
        ];
        return ! empty( $data ) ? wp_parse_args( $return, $data ) : $return;
    }

    protected function get_post( $post = NULL, $get_id = FALSE ) {
        if ( empty( $post ) && ! defined( 'DOING_AJAX' ) ) return;
        if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return $this->get_post_ajax( $get_id );
        } else {
            if ( is_numeric( $post ) ) $post = $get_id ? (int) $post : get_post( $post );
            if ( ( $post instanceof \WP_Post ) ) return $get_id ? (int) $post->ID : $post;
        }
    }

    protected function get_post_ajax( $get_id = FALSE ) {
        $postid = (int) gmce_get_post( 'gmce_post', 'int' );
        if ( $get_id ) return $postid;
        if ( $postid > 0 ) $post = get_post( $postid );
        if ( ( $post instanceof \WP_Post ) ) return $post;
    }

    protected function get_vote_down_info_ajax() {
        $reason = gmce_get_post( 'gmce_reason' );
        $raw = gmce_get_post( 'gmce_reason_content', 'string', TRUE );
        $content = wp_kses( nl2br( $raw ), $this->settings->get( 'allowed_html', [ ] ) );
        return [ 'reason' => $reason, 'content' => $content ];
    }

    protected function check_vote( $up_down, $votes, $post, $reason = NULL ) {
        if ( empty( $votes ) ) return 'error';
        if ( $this->user->is_author ) return 'author';
        $locked = (bool) $votes['locked'];
        if ( $locked && ( ! $this->user->is_admin || ( $reason !== '_final_revision_' ) ) ) {
            return 'locked';
        }
        if ( ! $locked ) {
            if ( ! $this->user->can_vote( $post, $up_down ) ) return 'notcan';
            if ( $this->user->had_voted( $votes, 'triple' ) ) return 'triple';
            if ( $this->user->had_voted( $votes, $up_down ) ) return 'already';
            if ( $this->user->had_voted( $votes, 'down' ) ) return 'already_down';
        }
    }

    protected function get_undo_votes( $votes ) {
        $i = array_search( $this->user->ID, $votes['up'] );
        unset( $votes['up'][$i] );
        $votes['triple'][] = (int) $this->user->ID;
        return $votes;
    }

    protected function votes_count( $votes ) {
        $approved = count( $votes['up'] );
        $denied = count( $votes['down'] );
        $final = count( $votes['final_down_users'] );
        $total = $approved + $denied + $final;
        return [
            'approved' => $approved,
            'denied'   => $denied,
            'final'    => $final,
            'total'    => $total,
            'fixed'    => (int) $votes['fixed'],
            'unfixed'  => absint( $total - $approved - $votes['fixed'] )
        ];
    }

    protected function approvers_and_deniers( $votes ) {
        $editors = [ 'approvers' => [ ], 'denyers' => [ ], 'final_editors' => [ ] ];
        $up = array_map( 'intval', array_filter( (array) $votes['up'] ) );
        $down = array_map( 'intval', array_filter( (array) $votes['down'] ) );
        $finals = array_map( 'intval', array_filter( (array) $votes['final_down_users'] ) );
        $ids = array_unique( array_merge( $up, $down, $finals ) );
        if ( empty( $ids ) ) return $editors;
        sort( $ids );
        $k = md5( implode( '|', $ids ) );
        $users = wp_cache_get( $k, 'gmce' );
        if ( $users === FALSE ) {
            $uq = new \WP_User_Query( [ 'include' => $ids ] );
            $users = $uq->get_results();
            wp_cache_set( $k, $users, 'gmce' );
        }
        foreach ( (array) $users as $user ) {
            $url = $this->urleize_user( $user );
            if ( in_array( $user->ID, $up ) ) $editors['approvers'][$user->ID] = $url;
            if ( in_array( $user->ID, $down ) ) $editors['denyers'][$user->ID] = $url;
            if ( in_array( $user->ID, $finals ) ) $editors['final_editors'][$user->ID] = $url;
        }
        return $editors;
    }

    protected function urleize_user( $user ) {
        $format = '<a href="%s" data-uid="%d">%s</a>';
        $url = esc_url( get_author_posts_url( $user->ID ) );
        return sprintf( $format, $url, $user->ID, esc_html( $user->display_name ) );
    }

    protected function votes_reasons( $votes ) {
        $reasons = [ 'reasons' => array_count_values( $votes['down_reasons'] ) ];
        $final = count( $votes['final_down_users'] );
        $reasons['final_reasons'] = ( $final > 0 ) ? [ '_final_revision_' => $final ] : [ ];
        return $reasons;
    }

}