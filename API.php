<?php
/**
 * GM Commununity Editors API
 *
 * @package GMCommunityEditor
 * @author Giuseppe Mazzapica <giuseppe.mazzapica@gmail.com>
 * @version 1.1.0
 * @license GPLv2
 */
use Brain\Container as Brain;

/**
 * Get the community revisions
 *
 * It's sort of get_posts specific for revisions.
 * accepted keys (with defaults) are:
 *
 * <ul>
 * <li>**`'posts'`** (default `null`): limit the revision retrieved to an array of post IDs</li>
 * <li>**`'user'`** (default `null`): limit the revision retrieved to an user</li>
 * <li>**`'status'`** (default `'publish'`): retrieve only revision with a specifi status</li>
 * <li>**`'exclude_final'`** (default `true`): exclude final revision from results</li>
 * </ul>
 *
 * @param array $arg the arguments array
 * @return array array of revisions
 * @access public
 * @since 1.0.0
 *
 */
function gmce_get_revs( $args = array () ) {
    $api = Brain::instance()->get( 'api' );
    return $api->get_revs( $args );
}

/**
 * Get the community revisions for an user
 *
 * @param int|object $user the user which retrieve revisions. May be user id or user object.
 *                   If empty, will be the current user if one.
 * @param string $status limit revision to a status. Should be 'publish', 'trash' or 'any'
 * @param bool $no_final exclude final revisions from results
 * @return array|bool array of revisions for given user if exist, false if not.
 * @access public
 * @since 1.0.0
 *
 */
function gmce_get_user_revs( $user = 0, $status = 'publish', $no_final = true ) {
    if ( empty( $user ) ) $user = \wp_get_current_user();
    if ( \is_numeric( $user ) ) $user = (int) $user;
    if ( ! \is_object( $user ) && ! \is_int( $user ) ) return;
    if ( \is_object( $user ) && ( ! isset( $user->ID ) || $user->ID <= 0 ) ) return;
    $api = Brain::instance()->get( 'api' );
    return $api->get_user_revs( $user, $status, $no_final );
}

/**
 * Get the community revision of for a post
 *
 * @param int|object $post retrieve revisions from this post. May be a post id or a post object.
 *                         By default is the current post.
 * @param string $status limit revision to a status. Should be 'publish', 'trash' or 'any'.
 * @param bool $no_final exclude final revisions from results
 * @return array|bool array of revisions for given post if exist, false if not
 * @access public
 * @since 1.0.0
 *
 */
function gmce_get_post_revs( $post = 0, $status = 'publish', $no_final = true ) {
    if ( empty( $post ) ) global $post;
    if ( ! is_numeric( $post ) && ! $post instanceof \WP_Post ) return false;
    $api = Brain::instance()->get( 'api' );
    return $api->get_post_revs( $post, $status, $no_final );
}

/**
 * Get the community votes info for an user
 *
 * This function can be used to retrieve all the information regarding votes casted by an user,
 * or a single 'piece' of information. All the info available are:
 *
 * <ul>
 * <li> **`'approvals'`** => number of approvals by user to published posts (confirmed)</li>
 * <li> **`'revisions'`** => number of revisions by user to published posts (confirmed)</li>
 * <li> **`'total'`** => number of revisions by user to published posts (confirmed)</li>
 * <li> **`'approvals_now'`** => number of approvals by user including ones to pending posts</li>
 * <li> **`'revisions_now'`** => number of revision by user including ones to pending posts</li>
 * <li> **`'total_now'`** => number of all votes by user including ones to pending post</li>
 * <li> **`'final'`** => number of final revisions to published and pending posts</li>
 * </ul>
 *
 * When the **`$which`** argument is one of the previous string, only the information is returned,
 * if **`$which`** is null or false, all informations are returned as array
 *
 * @param int|object $user the user which retrieve votes info. May be an user id or an user object.
 *                   If empty, will be the current user if one.
 * @param string|null $which the info piece to get
 * @return array|int|bool when a valid $which arg is give returns the specific piece of data,
 *                        when $which is null or false, it returns an array with all info.
 *                        If user does not exists or is invalid return false.
 * @access public
 * @since 1.0.0
 */
function gmce_get_user_votes( $user = 0, $which = null ) {
    if ( empty( $user ) ) $user = \wp_get_current_user();
    if ( \is_numeric( $user ) ) $user = (int) $user;
    if ( ! \is_object( $user ) && ! \is_int( $user ) ) return;
    if ( \is_object( $user ) && ( ! isset( $user->ID ) || $user->ID <= 0 ) ) return;
    $api = Brain::instance()->get( 'api' );
    return $api->get_user_votes( $user, $which );
}

/**
 * Get the community votes info for a post.
 *
 * This function can be used to retrieve all the information regarding votes casted to a post
 * or a single 'piece' of information. All the info available are:
 *
 * <ul>
 * <li>**`'approvals'`** => number of approvals for the post</li>
 * <li>**`'revisions'`** => number of revisions for the post</li>
 * <li>**`'final'`** => number of final revisions (fixed and unfixed)</li>
 * <li>**`'approvers'`** => array of post approvers profile links, where array keys are users IDs</li>
 * <li>**`'revisers'`** => array of post revisers profile links, where array keys are users IDs</li>
 * <li>**`'revisers'`** => array of post revisers profile links, where array keys are users IDs</li>
 * <li>**`'unfixed'`** => number of unfixed revisions (regular and final)</li>
 * <li>**`'fixed'`** => number of fixed revisions (regular and final)</li>
 * <li>**`'total'`** => total votes: approvals + regular revisions + final revisions</li>
 * <li>**`'reasons'`** => array of revision reasons, keys are revision slugs, values revision count</li>
 * <li>**`'score'`** => post score</li>
 * <li>**`'locked'`** => an empty string when post is not locked, '1' otherwise</li>
 * </ul>
 *
 * @param int|object $post the post which retrieve votes info. May be post id or post object.
 *                   By default the current post.
 * @param string|null $which the user info to get, can be of the following:
 * @return mixed when a valid $which arg is give returns the specific piece of data,
 *               when $which is null or false, it returns an array with all info.
 * @access public
 * @since 1.0.0
 */
function gmce_get_post_votes( $post = 0, $which = null ) {
    if ( empty( $post ) ) global $post;
    if ( ! is_numeric( $post ) && ! $post instanceof \WP_Post ) return false;
    $api = Brain::instance()->get( 'api' );
    return $api->get_post_votes( $post, $which );
}

/**
 * Retrieve an array of all editors for the post, both approvers and revisers
 *
 * This function can be used to retrieve an array of all the editors for a post, or it can be used
 * to return editors as a echoabkle string.
 * Post param is setted by default to the current post, so inside The Loop a typical use can be:
 *
 * <pre>
 * <p>
 * This post was edited by <?php echo gmce_get_post_editors(); ?>.
 * </p>
 * </pre>
 *
 * And this will return all the editors (approvers and revisers) separed by comma
 * If second param `$sep` is setted to false or null, the function will return an associative array
 * where keys are the user IDs and the values the related post archive url
 *
 * @param int|object $post the post which retrieve votes info. May be post id or post object.
 *                   By default is the current post.
 * @param string|bool $sep separator string or false to return the array
 * @return string|array all the editors profile links merged with given separator or,
 *                      if $sep argument is null or false, the array of all editors
 *                      where keys are user ids
 *                      and values users profile links
 * @access public
 * @since 1.0.0
 */
function gmce_get_post_editors( $post = 0, $sep = ', ' ) {
    if ( empty( $post ) ) global $post;
    if ( ! is_numeric( $post ) && ! $post instanceof \WP_Post ) return false;
    $api = Brain::instance()->get( 'api' );
    $all = $api->get_post_votes( $post );
    $editors = \array_unique( \array_merge( (array) $all[ 'revisers' ], (array) $all[ 'approvers' ] ) );
    if ( $sep === false ) return $editors;
    if ( \is_string( $sep ) ) return \implode( $sep, $editors );
}
