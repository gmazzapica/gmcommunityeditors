<?php
/*
 * Plugin Name: GM Community Editors
 * Plugin URI: http://zoomlab.it
 * Description: Allows posts to be community reviewed by site contributors with a special role.
 * Version: 1.2.0
 * Author: Giuseppe Mazzapica
 * Author URI: http://zoomlab.it
 * License: GPLv2
 *
 * Text Domain: gmce
 */
use Brain\Container as Brain;
if ( ! defined( 'ABSPATH' ) ) die();

$path = plugin_dir_path( __FILE__ );

load_plugin_textdomain( 'gmce', FALSE, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

require_once $path . 'vendor/autoload.php';

add_action( 'brain_init', function( $brain ) use( $path ) {
    GMCommunityEditors( $brain, $path );
} );

add_filter( 'gmce_supported_plugins', function( $plugins = [ ] ) {
    $path = plugin_dir_path( __FILE__ ) . 'allowed_plugins.php';
    if ( ! is_file( $path ) ) {
        $path = plugin_dir_path( __FILE__ ) . 'allowed_plugins.defaults';
        if ( ! is_file( $path ) ) return;
    }
    $add = (array) include( $path );
    if ( is_array( $add ) ) $plugins = array_filter( array_merge( $plugins, $add ) );
    return $plugins;
} );

add_filter( 'wp_link_pages', function( $html ) {
    if ( is_preview() ) {
        $matches = [ ];
        preg_match_all( '/href=["|\']([^\s"|\']+)/', $html, $matches );
        if ( ! isset( $matches[ 1 ] ) || empty( $matches[ 1 ] ) ) return $html;
        foreach ( $matches[ 1 ] as $url ) {
            $url_dec = html_entity_decode( $url );
            $new_url = add_query_arg( array ( 'preview' => 'true' ), $url_dec );
            $html = str_replace( $url, $new_url, $html );
        }
    }
    return $html;
} );

add_action( 'gmce_cron', function( $cron ) {
    $cron->addTask( 'twicedaily', [ Brain::instance()->get( 'auto_publisher' ), 'schedule' ] );
    $cron->addTask( 'daily', [ Brain::instance()->get( 'thumb_votes' ), 'cleanup_transient' ] );
} );

register_activation_hook( __FILE__, function() use( $path ) {
    Brain::flush();
    $brain = Brain::boot( new \Pimple );
    $s = $brain->get( 'settings' );
    $s->load_file( $brain->get( 'path' ) . 'configs.php' );
    $installer = new GMCE\Installer( $s, $brain->get( 'cron' ) );
    $installer->install();
} );


register_deactivation_hook( __FILE__, function() use( $path ) {
    Brain::flush();
    $brain = Brain::boot( new \Pimple );
    $s = $brain->get( 'settings' );
    $s->load_file( $brain->get( 'path' ) . 'configs.php' );
    $installer = new GMCE\Installer( $s, $brain->get( 'cron' ) );
    $installer->unistall();
} );

function GMCommunityEditors( Brain $brain, $path ) {
    if ( ! is_string( $path ) ) return;
    $brain[ 'path' ] = $path;
    $brain[ 'url' ] = plugins_url( '/', $path . '/plugin.php' );
    $brain[ 'cpt' ] = 'rev_vote';
    $brain[ 'tax' ] = 'rev_reason';
    $brain->addModule( new GMCE\BrainModule );
}

if ( ! function_exists( 'wp_normalize_path' ) ) {

    function wp_normalize_path( $path ) {
        return str_replace( '\\', '/', preg_replace( '|/+|', '/', $path ) );
    }

}
if ( ! function_exists( 'wp_register_plugin_realpath' ) ) {

    function wp_register_plugin_realpath( $file ) {
        global $wp_plugin_paths;
        static $wp_plugin_path, $wpmu_plugin_path;
        if ( ! isset( $wp_plugin_path ) ) {
            $wp_plugin_path = wp_normalize_path( WP_PLUGIN_DIR );
            $wpmu_plugin_path = wp_normalize_path( WPMU_PLUGIN_DIR );
        }
        $plugin_path = wp_normalize_path( dirname( $file ) );
        $plugin_realpath = wp_normalize_path( dirname( realpath( $file ) ) );
        if ( $plugin_path === $wp_plugin_path || $plugin_path === $wpmu_plugin_path ) {
            return false;
        }
        if ( $plugin_path !== $plugin_realpath ) {
            $wp_plugin_paths[ $plugin_path ] = $plugin_realpath;
        }
        return true;
    }

}